NTHUOJ
=======
NTHU OJ is an online judge project currently maintained by users in National Tsing Hua University (Taiwan).
THis project provides an relatively simple skeleton of a real , complicated online judge system . 
Currently , we only support C / C++ code sumission and judging, and we're working on JAVA .

Though this system is originally developed aimed for ACM-ICPC competitors training , it can also be used for other educational purpose(Homework or Exam , as long as they have fixed answers).

Get our project:
---
Our project consists of three parts,  Web Interface , Task Dispatcher , and Judge System , The following text is about how to install these parts respectively.

	git clone https://bitbucket.org/bruce3557/nthuoj-open
Web:
---
* Dependencies: 
	* openssh-server 
	* php5 
	* apache2 
	* nfs-kernel-server 
	* php5-mysql 
	* mysql-server
	* phpmyadmin
	* g++ 
	* php5-curl
1. clone the project 

	sudo cp -r frontend/* /var  ( or any folder one directory above)

edit nthuoj.ini for changing the ip , username and password of the database engine.  

	sudo chmod +x setup.sh
	sudo ./setup.sh

Dispatcher:
---
* move ‘dispatcher’ folder to places you like
* write ‘machine.cofig’ (under ‘dispatcher’ folder):
Write this file to indicate the judge vms’ name and their ports . abide by the following format:
	[vm name] [port]

ex. 
* nthuoj-vm1 192.168.136.101
* nthuoj-vm2 192.168.136.102
* nthuoj-newoj 192.168.136.103
* run dispatcher: go to ‘dispatcher’ folder and:
	
	sudo php dispatcher.php &

Backend:
---
* Dependencies:  
	* openssh-serve
	* php5
	* apache2
	* nfs-kernel-server
	* php5-mysql
	* mysql-server
	* phpmyadmin
	* g++
	* php5-curl

1. Run the install.sh(in backend/VM) on the VM which you want to use as a Judge VM and follow the instructions to type some necessary information. 
2. Mount the source, testcase, and speJudge folders in judge VM to the corresponding folders in dispatcher VM. 

	* Give some IP right to mount the files on dispatcher VM. Add a file called exports in /etc on dispatcher VM with the content:

[folder path saved in dispatcher VM] [Judge VM IP] (rw,sync,no_subtree_check)
```
ex: /home/acm/Documents/nthuoj/judge/source 192.168.136.2(rw,sync,no_subtree_check)

* Give commands in every Judge VM to mount the folders:
	mount -t nfs [dispatcher IP]: [folder path saved in dispatcher VM] [folder path saved in judge VM]

ex: mount -t nfs 192.168.136.1: /home/acm/Documents/nthuoj/judge/source /home/nthuoj/judge/source
* For outsideconnection, run moniteruva.php posticpc.php crawlicpc.php on dispatcher VM: 

	sudo php moniteruva.php&
	sudo php posticpc.php&
	sudo php crawlicpc.php&
	
