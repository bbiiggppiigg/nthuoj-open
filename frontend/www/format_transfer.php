<?php
    session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

    $tpl = new Handler("Format Transformation", "format_transfer.tpl");
    $tpl->display("base.html");
?>
