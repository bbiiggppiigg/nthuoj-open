<?php
/****************************
forgetPassword.php
This page is used in case of forgetting password.
New password would be send to the email.
Checks POST parameter 'submit' and 'uid' to determined whether to update password or not.
*****************************/
  session_start();
  include_once("lib/base.php");
  include_once("lib/database_tools.php");
  include_once("lib/handler.php");
  $msg="";
  $tpl = new Handler("Forget Password", "forgetPassword.tpl");
  if( isset($_POST['submit']) ) {
    $uid = $_POST['uid'];
    $con = get_database_object();
    
    $query = "SELECT id, email FROM users WHERE id = '$uid'";
    $result = mysql_query($query) or die('Query failed.'.mysql_error());

    $user_info = mysql_fetch_array($result, MYSQL_ASSOC);
    if(!isset($user_info['email']) || $user_info['email'] == '') {
      $msg = "No email address, please contact manager.";
    }
    $email = $user_info['email'];
    $new_passwd = "";
    $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $len = strlen($chars);
    for($i=0;$i<6;++$i)
      $new_passwd .= $chars[rand() % $len];
    
    $query = "UPDATE users SET password = PASSWORD('$new_passwd') 
      WHERE id = '$uid'";
    mysql_query($query) or die(mysql_error());

    $headers = "From: judge@acm.cs.nthu.edu.tw";
    $subject = "Reset Password";
    $msg = "Dear user,\n Your password has been changed to '$new_passwd'.\n\n -NTHUOJ TEAM";
    if(mail("$email", "$subject", "$msg", "$header"))
      $msg = "Reset password. Please check your email.";
    else
      $msg = "Reset failed";
    mysql_close($con);
  }
  $tpl->assign('msg', $msg);
  $tpl->display("base.html");

?>
