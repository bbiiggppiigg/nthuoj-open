/**********************************
npp_editor.php
This gives some notepad++ download site.
No parameter is required.
**********************************/
<?php
    session_start();
    include_once("lib/base.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

    $tpl = new Handler("Notepad++", "npp_editor.tpl");
    $tpl->display("base.html");
?>
