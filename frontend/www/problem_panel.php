<?php
/****************************************
problem_panel.php
This gives a page that shows all problems, and could add new problem.
Checks GET parameter 'del' to delete a problem.
Checks POST parameter 'submit' and 'pid' to add new problem. 
*****************************************/

	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
    if( !check_admin() )
        die("You have no judge account");

	$errors = array();
	$message = '';

    $tpl = new Handler("Problem Panel", "problem_panel.tpl");

	if(isset($_GET['del'])) {
        $con = get_database_object();

		$query = "DELETE FROM submissions WHERE pid = ".$_GET['del'];
		mysql_query($query) or die('query failed'.mysql_error());

		$query = "DELETE FROM problems WHERE pid = ".$_GET['del'];
		mysql_query($query) or die('query failed'.mysql_error());

		$query = "DELETE FROM pid_cid WHERE pid = ".$_GET['del'];
		mysql_query($query) or die('query failed'.mysql_error());

		$query = "DELETE FROM mapping WHERE pid = ".$_GET['del'];
		mysql_query($query) or die('query failed'.mysql_error());

		$path = "/var/nthuoj/testdata/".$_GET['del'].".in";
		if(file_exists($path))
			unlink($path);
		$path = "/var/nthuoj/testdata/".$_GET['del'].".out";
		if(file_exists($path))
			unlink($path);

		header('Location: problem_panel.php');		
		exit;
	}

    $con = get_database_object();
	if(isset($_POST['submit'])) {
		$message = check_pid($_POST['pid']);
		if($message == '') {
			$query = "SELECT pid FROM problems WHERE pid = ".$_POST['pid'];
			$result = mysql_query($query) or die('query failed'.mysql_error());
			if(mysql_num_rows($result) == 0) {
				$query = "INSERT INTO problems (pid) 
                          VALUES (".$_POST['pid'].")";
				mysql_query($query) or die('query failed'.mysql_error());
				mysql_close($con);
				header('Location: edit_problem.php?pid='.$_POST['pid']);
				exit;
			} else {
				$message = 'Problem ID already exists.';
			}
		}
	}

	$query = "SELECT * FROM problems ORDER BY pid desc";
	$result = mysql_query($query) or die("Query failed".mysql_error());

    $rs = array();
	while($row = mysql_fetch_array($result, MYSQL_ASSOC))
        array_push($rs, $row);
    $tpl->assign("rs", $rs);
    $tpl->assign("msg", $message);
    
	mysql_close($con);
    $tpl->display("base.html");
?>
