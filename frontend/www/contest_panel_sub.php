<?php
/*********************************************************
contest_panel_sub.php
This gives a page to update a contest.
All details of a contest is set in here.

Checks POST parameter 'update' as a sign to update contest 'cname', 'start_time', 'end_time'.
Checks GET parameter 'pid' to del problem in the contest.
Checks GET parameter 'uid' to del co-owner of the contest.
Checks POST parameter 'submit' as a sign to add a problem to the contest.
Checks POST parametr 'add-coowner' to add a cowner to the contest.
*********************************************************/


	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
    $isowner=false;
	if( !check_coowner() )
        die("You have no judge permission");

	function check_datetime($s1) {
		$vowel  = array("T");
		$s1 = str_replace($vowel," ",$s1);
		if( ($array = date_parse($s1))==false)
			return false;
		if(!(isset($array['error'])||isset($array['warning']))) {
			$s2 = sprintf("%d-%02d-%02d %02d:%02d:%02d",
                $array['year'], $array['month'], $array['day'], $array['hour'],
                $array['minute'], $array['second']);
			return $s2;
		} else {
			return false;
        }
	}
	
    $tpl = new Handler("NTHU Online Judge", "contest_panel_sub.tpl");
	$message = '';
	$message2 =  '';
	if(isset($_POST['update'])) {
		if(check_cname($_POST['cname'])) {
			if(!$_POST['start_time'] || ($st=check_datetime($_POST['start_time']))!=false) {
				if(!$_POST['end_time'] || ($et=check_datetime($_POST['end_time']))!=false) {
						if(isset($_POST['freeze']) && check_freeze($_POST['freeze'],$st,$et)!=false) {
                            $con = get_database_object();
							$query = "UPDATE contest SET freeze = ".$_POST['freeze'].",cname = '".$_POST['cname']."', start_time = '".$_POST['start_time']."', end_time = '".$_POST['end_time']."', result = '".$_POST['result']."' WHERE cid = ".$_GET['cid'];
							mysql_query($query) or die('query failed'.mysql_error());
							mysql_close($con);
							$message2 .=  "alert('Update Succeed');";
						} else {
							$message .= 'Invalid freeze time.';
                        				}
				} else {
					$message .= 'Invalid end time.';
		                }
			} else {
				$message .= 'Invalid start time.'.$_POST['start_time'];
            		}
		} else {
			$message .= 'Invalid contest name.';
		}
	    echo $message;
	}


	if(isset($_GET['pid'])) {
        $con = get_database_object();
		$query = "DELETE FROM pid_cid WHERE cid = ".$_GET['cid']." AND pid = ".$_GET['pid'];
		mysql_query($query) or die('query failed'.mysql_error());
		mysql_close($con);

		header('Location: contest_panel_sub.php?cid='.$_GET['cid']);		
		exit;
	}
	if(isset($_GET['uid'])){
		$con = get_database_object();
		$query = "DELETE FROM contest_coowner WHERE cid = ".$_GET['cid']." AND id = '".$_GET['uid']."'";
		mysql_query($query) or die('query failed'.mysql_error());
		mysql_close($con);
		header('Location: contest_panel_sub.php?cid='.$_GET['cid']);		
		exit;
	}
	if(isset($_POST['submit'])) {
        $con = get_database_object();

		$message = check_pid($_POST['pid']);
		if($message == '') {
			$query = "SELECT pid FROM problems WHERE pid = ".$_POST['pid'];
			$result = mysql_query($query) or die('query failed'.mysql_error());
			if(mysql_num_rows($result) == 1) {
				$query = "SELECT pid FROM pid_cid  WHERE cid = ".$_GET['cid']." AND pid = ".$_POST['pid'];
				$result = mysql_query($query) or die('query failed'.mysql_error());
				if(mysql_num_rows($result) == 0) {
					$query = "INSERT INTO pid_cid (pid,cid) VALUES (".$_POST['pid'].",".$_GET['cid'].")";
					mysql_query($query) or die('query failed'.mysql_error());
				} else {
					$message = 'Problem already exist in this contest';
                }
			} else {
				$message = 'Problem does not exist.';
            }
		}
		mysql_close($con);
	}
	
    $con = get_database_object();
	if(isset($_POST['add-coowner'])){
		if(!isset($_POST['uid']) || $_POST['uid']=="" || $_POST['uid']==NULL){
			$message = "Coowner must not be null";
		}else{
			$query = "Select user_level from users where id = '{$_POST['uid']}'";
			//echo $query;
			$result = mysql_query($query) or die("Coowner must be a user!!!".mysql_error());
			$row = mysql_fetch_array($result,MYSQL_ASSOC);
			
			if(!check_coowner($row['user_level'])){
				$message = "Contest coowner must have coowner permission!!!";
			}else{
				$query = "Insert into `contest_coowner` (`cid`,`id`) values ({$_GET['cid']},'"."{$_POST['uid']}"."')";
				$result = mysql_query($query) or die("Query failed".mysql_error());
			}
		}
	}
	$query = "SELECT cname,start_time,end_time,freeze,result FROM contest WHERE cid =".$_GET['cid'];
	$result = mysql_query($query) or die("Query failed".mysql_error());
	$row = mysql_fetch_array($result,MYSQL_ASSOC);
	if(!$row)
		die("Contest ID does not exist.");
	
    if(isset($_GET['pid']))
	$tpl->assign("pid", $_GET['pid']);
    if(isset($_GET['cid']))
	$tpl->assign("cid", $_GET['cid']);
    if($_GET['cid']!=1) {
        $tpl->assign("cname", htmlspecialchars_decode($row['cname'], ENT_QUOTES));
        $vowel = array(" ");
		$tpl->assign("start_time", str_replace($vowel,"T",$row['start_time']));
		$tpl->assign("end_time", str_replace($vowel,"T",$row['end_time']));
        $tpl->assign("freeze", $row['freeze']);
        $tpl->assign("res", $row['result']);
    } 
	$query = "SELECT problems.pid,pname FROM problems INNER JOIN pid_cid ON problems.pid = pid_cid.pid WHERE pid_cid.cid = ".$_GET['cid']." ORDER BY problems.pid";
   	$result = mysql_query($query) or die("Query failed".mysql_error());
	$rs = array();
   	while($row = mysql_fetch_array($result, MYSQL_ASSOC))
        array_push($rs, $row);
   	$co = array();
	$query = "SELECT id from contest_coowner where cid = {$_GET['cid']}";
	$result = mysql_query($query) or die("Query failed".mysql_error());
	while($row = mysql_fetch_array($result, MYSQL_ASSOC))
		array_push($co, $row);
	$query = "SELECT owner from contest where cid = {$_GET['cid']}";
	$result = mysql_query($query) or die("Query failed".mysql_error());
	
	$row = mysql_fetch_array($result,MYSQL_ASSOC);
	
	
	if(strcmp($row['owner'],$_SESSION['uid'])==0){
		$isowner = true;
	
	}
	$tpl->assign("msg2",$message2);
	$tpl->assign("msg", $message);
	$tpl->assign("rs", $rs);
	$tpl->assign("co", $co);
	$tpl->assign("isowner",$isowner);
    $tpl->display("base.html");
?>
