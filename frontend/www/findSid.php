<?php
/****************************************
findSid.php
This php is used to find submission ID.
Sid would be search by uid or pid.
Checks GET parameter 'uid', 'pid' as search key.
POST parameter 'submit' act as another sign to search.
If there's 'submit' POST parameter, could be search by POST parameter 'uid', 'pid'.
****************************************/

	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
    if( !check_admin() )
        die("You have no admin account");
	
	$pid = "";
	$uid = "";
	$sub = "";
	$state = 0;
	if(isset($_GET["pid"]))
		$pid = $_GET["pid"];
	if(isset($_GET["uid"]))
		$uid = $_GET["uid"];
	if(isset($_POST['submit'])) {
		$pid = $_POST['pid'];
		$uid = $_POST['uid'];
        /*
		if($pid!='' && $uid!='')
			header('Location: findSid.php?pid='.$_POST['pid'].'&uid='.$_POST['uid']);
		else if($pid!='')
			header('Location: findSid.php?pid='.$_POST['pid']);
		else if($_GET["pid"]!='' && $uid!='')
			header('Location: findSid.php?pid='.$_GET['pid'].'&uid='.$_POST['uid']);
        else 
			header('Location: findSid.php');
		exit;
        */
	}

	if($pid!='' && $uid !=''){
		$state = 1;
		if(!$con)
			die('Could not connect: '. mysql_error());
		mysql_select_db("nthuoj", $con);
		if($_GET["all"])
			$sql = "select sid, uid, source, status, date from submissions where pid = $pid and uid = '$uid' order by date asc";
		else 
			$sql = "select sid, uid, source, status, date from submissions where pid = $pid and status = 'Accepted' and uid = '$uid' group by uid order by uid asc, date asc";
		$rs = mysql_query($sql) or die('Query failed');
		$sub = array();
		$i = 0; 
		while ($data = mysql_fetch_array($rs)) {
			$sub[$i]['sid'] = $data['sid'];
			$sub[$i]['date'] = $data['date'];
			$sub[$i]['status'] = $data['status'];
			++$i;
		}
		mysql_close($con);
	}else if($pid!=''){
		$state = 2;
		if(!$con)
			die('Could not connect: '. mysql_error());
		mysql_select_db("nthuoj", $con);
		$rs = mysql_query($sql);

		/////////////////////////////////////////
		
		$sql = "select sid, uid,source, date from submissions where pid = $pid and status = 'Accepted' group by uid order by uid asc, date asc";
		$rs = mysql_query($sql) or die('Query failed');
		$dead1 = strtotime($question[$pid][0]);
		$dead2 = strtotime($question[$pid][1]);
		$i=0;
		$sub = array();
		while ($data = mysql_fetch_array($rs)) {
			$sDate = strtotime($data['date']);
			$uid = strtolower ($data['uid']);
			$sub[$i]['uid'] = $data['uid'];
			$sub[$i]['sid'] = $data['sid'];
			$sub[$i]['source'] = $data['source'];
			if($sub[$i]['sid']=='')$sub[$i]['sid'] = '-1';
			++$i;
		}
		mysql_close($con);
	} else if($uid != '') {
        $state = 3;
        if(!$con)
            die('Could not connect: '. mysql_error());
        mysql_select_db("nthuoj", $con);
        $sql = "SELECT pid, sid, uid, source, date, status FROM submissions WHERE uid='$uid' ORDER BY date desc";
        $rs = mysql_query($sql) or die('Query failed');
        $sub = array();
        $i = 0;
        while( $data = mysql_fetch_array($rs) ) {
            //$sDate = $strtotime($data['date']);
            $uid = strtolower($data['uid']);
            $sub[$i]['uid'] = $data['uid'];
            $sub[$i]['sid'] = $data['sid'];
            $sub[$i]['pid'] = $data['pid'];
            $sub[$i]['source'] = $data['source'];
            $sub[$i]['status'] = $data['status'];
            $sub[$i]['date'] = $data['date'];
            if($sub[$i]['sid'] == '') $sub[$i]['sid'] = '-1';
            ++$i;
        }
        mysql_close($con);
    }
	$tpl = new Handler("findSid", "findSid.tpl");
	
	$tpl->assign("pid", $pid);
	$tpl->assign("uid", $uid);
	$tpl->assign("state", $state);
	$tpl->assign("sub", $sub);
    $tpl->display("base.html");
?>
