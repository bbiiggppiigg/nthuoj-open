<?php
	session_start();
    include_once("lib/base.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
    
    
    $con = get_database_object();
    $i=0;
	if(!isset($_GET["pid"]))
		die('Problem ID does not exist');
	$pid = $_GET["pid"];
	if(!is_numeric($pid))
		die('Problem ID does not exist');

    $tpl = new Handler("Rank ".$pid, "rank.tpl");

	$query = "SELECT * FROM problems WHERE pid =".$pid;
	$result = mysql_query($query) or die("Query failed".mysql_error());
	$row = mysql_fetch_array($result,MYSQL_ASSOC);
	if(!$row)
		die('Problem ID does not exist');
	$pname = $row['pname'];
	$query = "SELECT distinct s.uid uid
	          FROM submissions s 
              INNER JOIN users u ON s.uid = u.id
	          WHERE u.user_level < 10 
                AND s.pid = $pid 
                AND s.status = \"Accepted\"
             ";
	$result = mysql_query($query) or die("Query failed".mysql_error());
	$arr = array();
	while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		if ($row['uid'] == 'nthucs') continue;
		$query = "SELECT cpu, date 
                  FROM submissions 
                  WHERE pid = $pid 
                    AND status = \"Accepted\" 
                    AND uid = \"".$row['uid']."\" 
                  ORDER BY cpu asc, date ASC 
                  LIMIT 1";
		$r2 = mysql_query($query) or die("Query failed".mysql_error());
		$row2 = mysql_fetch_array($r2, MYSQL_ASSOC);
		$arr[$i++] = array($row['uid'], $row2['cpu'], $row2['date']);
	}	
	mysql_close($con);

	function cmp($a, $b) {
	    if ($a[0] == $b[0]) return 0;
		if ($a[1] == $b[1]) return $a[2] < $b[2] ? -1 : 1;
		return ($a[1] < $b[1]) ? -1 : 1;
	}
	usort($arr, "cmp");
    
    $tpl->assign("pid", $pid);
    $tpl->assign("pname", $pname);

    $rs = array();
	$i = 1;
	foreach ($arr as $item) {
        $row = array();
        $row['rank'] = $i;
        $row['uid'] = $item[0];
        $row['cpu'] = $item[1];
        $row['date'] = $item[2];
        array_push($rs, $row);
		++$i;
	}
    $tpl->assign("rs", $rs);
    $tpl->display("base.html");
?>

