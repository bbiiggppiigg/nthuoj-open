<?php
/****************
 * scoreboard_tools.php
 * This provides a function library related to scoreboard.
 * **************/

/*************************************************
 * 'passNewProblem'  checks whether a new problem is newly passed (AC).
 * If it is, return true. If already passed or not passed yet, return false.
 * *********************************************/
	function passNewProblem($all, $row)
	{	
		if(isset( $all[$row['uid']][$row['pid']]['accepted']) 
		 && $all[$row['uid']][$row['pid']]['accepted']==true )
			return false; /*already passed*/
		else if( passProblem($row) )
			return true; 
		else	return false;
	}
	
/***********************************************************
 * 'passProblem' checks whether a status means AC.
 * Checking by formate 'a/b', if a==b means AC.
 * If AC, return true. Else return false.
 * ********************************************************/
	function passProblem($row)
	{
		$str = $row['status'];
		$a = strtok($str, "/");
		$b = strtok("/");
		if( $a===$b )
			return true;
		else return false;
	}
	
/**********************************************************
 * 'getPenalty' counts the penalty of a user of a problem in between start time and end time.
 * (Input: $uid for user, $pid for problem, $start_time, $end_time)
 * Penalty of a submission = (#NOT AC) * 20 + ( Submit time - Start time)
 * Total penalty = sum of all submission penalty.
 * Total penalty is returned.
 * *******************************************************/
	function getPenalty($uid, $pid, $start_time, $end_time)
	{
	    $con = get_database_object();
		$query =" SELECT * 
				FROM  `submissions` 
				WHERE  `uid` =  '".$uid."'
				AND pid = $pid
				AND  date  BETWEEN '$start_time' AND '$end_time'
				ORDER BY  `submissions`.`date` ASC  ";
		$result = mysql_query($query) or die("Query failed in getPenalty. ".$uid.$pid.mysql_error());
		$notAcCnt = 0;
		$penalty = 0;
		while( $row = mysql_fetch_array($result,MYSQL_ASSOC) ){
		    if(!checkStatusName($row['status']))	continue;
		    if(!timeSatisfied($row['date'], $start_time, $end_time)) continue;
		    
		    if(isAccepted($row['status'])){
		        $penalty += ( strtotime($row['date']) - strtotime($start_time) + $notAcCnt*20);
		        break;
		    }
		    else{
		        $notAcCnt += 1;
		    }
		}
		return $penalty;
	}
	
/********************************************************************
 * 'getHighestStatus' gives the best status of a user of a problem in the time period of start time and end time.
 * (Input: $uid for user, $pid for problem, $start_time, and $end_time)
 * If there's submission of the problem, and with status name in 'a/b' formate,
 * the best result status would be return.
 * Otherwise '--' would be return.
 * *****************************************************************/
	function getHighestStatus($uid, $pid, $start_time, $end_time)
	{
		$con = get_database_object();
		$query =" SELECT * 
				FROM  `submissions` 
				WHERE  `uid` =  '".$uid."'
				AND  `pid` = $pid 
				AND   date  BETWEEN '$start_time' AND '$end_time'
				ORDER BY  `submissions`.`sid` DESC  ";
		$result = mysql_query($query) or die("Query failed in getHighestStatus. ".$uid.$pid.mysql_error());
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		
		if($row){
			$max = $row['status'];
			/* ignore all useless status until find a satisfied one */
			while(!checkStatusName($row['status']) || !timeSatisfied($row['date'], $start_time, $end_time) ){
				$row = mysql_fetch_array($result,MYSQL_ASSOC);
				if($row) $max = $row['status'];
				else break;
			}
			/*compare all status and remain the highest score one*/
			while($row = mysql_fetch_array($result,MYSQL_ASSOC)){
				if(!checkStatusName($row['status']))	continue;
				if(!timeSatisfied($row['date'], $start_time, $end_time))	continue;
				$max = statusCMP($max, $row['status']);
			}
			if(!checkStatusName($max)) return "--";
			else return $max;
		}
		return "No Submission??";
	}
	
	
/****************************************************
 * 'timeSatisfied' check whether submit time is in time period of start time and end time.
 * (Input: $submit_time, $start_time, $end_time)
 * If is in, returns true. Otherwise, false is return.
 * ***************************************************/
	function timeSatisfied($submit_time, $start_time, $end_time)
	{
		$s = strtotime($start_time);
		$e = strtotime($end_time);
		$sb = strtotime($submit_time);
		if( $sb >= $s && $sb <= $e)
			return true;
		else return false;
	
	}
	
/******************************************************
 * 'statusCMP' compares two status with formate ' n1/ n2 '.
 * The status with hgher score would be return.
 * *****************************************************/
	function statusCMP($a, $b)
	{	
		$str1 = $a;
		$str2 = $b;
		$num1 = (int)strtok($str1, "/");
		$num2 = (int)strtok($str2, "/");
		if($num1>$num2)	return $a;
		else return $b;
	}
	
/**********************************************
 * checkStatusName checks a status name's formate.
 * Status is expected to be like ' num1 / num2 '.
 * Only this kind of status name should be put into consideration to handle.
 * This function checks '/' and make sure there are two items split by '/'.
 * If the formate is as expected return true. Otherwise, return false.
 *********************************************/
	function checkStatusName($s)
	{
		$arr = explode("/", $s);
		if( count($arr)>=2 && $arr[1] && (int)$arr[1]>0 )
			return true;
		else return false;
	}
	
/******************************************
 'cmp' function used for sorting.
 First compare the numbers of solved problems. 
 The higher the better.
 If tied, compare with the penalty. 
 The higher the worse here.
********************************************/
	function cmp($a, $b) {
		if($a['solved'] > $b['solved'])
			return -1;
		else if($a['solved'] < $b['solved'])
			return 1;
		else if($a['penalty'] < $b['penalty'])
		    return -1;
		else if($a['penalty'] > $b['penalty'])
		    return 1;
		return 0;
	}
?>