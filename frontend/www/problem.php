<?php
/***************************************
problem.php
This gives a page that shows problem.
Checks GET parameter 'pid' to know which problem to show.
****************************************/

	session_start();
    	include_once("lib/base.php");
    	include_once("lib/contest_lib.php");
	include_once("lib/problem_lib.php");
    	include_once("lib/database_tools.php");
    	include_once("lib/handler.php");

	$key = array();
	$title = array();
	$key[0] = 'description';
	$key[1] = 'input';
	$key[2] = 'output';
	$key[3] = 'sample_input';
	$key[4] = 'sample_output';
	$title[0] = 'Problem Description';
	$title[1] = 'Input';
	$title[2] = 'Output';
	$title[3] = 'Sample Input';
	$title[4] = 'Sample Output';
    
    $tpl = new Handler("Problem", "problem.tpl");
    $con = get_database_object();
	$message="";
    if(!isset($_GET["pid"]) || !is_numeric($_GET["pid"]))
		die('Problem ID does not exist');
	$pid = $_GET["pid"];
	$query = "SELECT * FROM problems WHERE pid =".$pid;
	$result = mysql_query($query) or die("Query failed".mysql_error());
	$row = mysql_fetch_array($result,MYSQL_ASSOC);
	if(!$row)
		die('Problem ID does not exist');
	$pname = $row['pname'];
    $special_judge = ( $row['special_judge'] != "" );
	
	/*default parent_pid value=0 --> dinamically let it point to itself
	if(0==$row[parent_pid]){
		$row[parent_pid]=$pid;
		$query = "UPDATE problems SET parent_pid= ".$pid." 
                      WHERE pid = ".$pid;
		$result = mysql_query($query) or die("Query failed :".mysql_error().$query);
	}*/
	
	/*not root problem --> get info from root prob*/
	$row_root =array();
	if(!checkIsRootPid($pid)){ 
		$parent_pid = getRootPid($pid);
		if(!is_numeric($parent_pid))
			die('Parent PID does not exist');
		$query = "SELECT * FROM problems WHERE pid = ".$parent_pid;
		$result = mysql_query($query) or die("Query failed :".mysql_error().$query);
		$row_root = mysql_fetch_array($result,MYSQL_ASSOC);
		$pname = $row['pname']? $row['pname'] : $row_root['pname'];
	}
	else $parent_pid = "root";
	
	$query = "SELECT * FROM problems 
              INNER JOIN pid_cid ON problems.pid=pid_cid.pid 
              INNER JOIN contest ON pid_cid.cid = contest.cid 
              WHERE (contest.cid = 1 OR NOW() >= contest.start_time) 
                AND problems.pid=".$_GET['pid'];
	$result = mysql_query($query) or die(mysql_error());
	if(mysql_num_rows($result) == 0
        && (!check_login() || !check_admin()) ) 
		die('Problem ID does not exist');

	$query = "SELECT * FROM testcases WHERE pid = $pid ORDER BY tid ASC";
	$result = mysql_query($query) or die(mysql_error());
	$time_limit = array();
	$memory_limit = array();
	while($rowForTestcase = mysql_fetch_array($result, MYSQL_ASSOC) ) {
		array_push($time_limit, $rowForTestcase['timeLimit']);
		array_push($memory_limit, $rowForTestcase['memoryLimit']);
	}
	
    $tpl->assign("pid", $pid);
    $tpl->assign("pname", htmlspecialchars_decode($pname, ENT_QUOTES));
    $tpl->assign("parent_pid", $parent_pid);
	$tpl->assign("time_limit", $time_limit);
    $tpl->assign("memory_limit", $memory_limit);
    $tpl->assign("special_judge", $special_judge);

    $rs = array();
	for($i = 0; $i <= 4; $i++){	
		$r2 = array();
        $r2["title"] = $title[$i];
		
		/* if there's problem information in this problem, use it. Otherwise, use information from root problem*/ 
		if($row[$key[$i]])
            $r2["content"] = htmlspecialchars_decode($row[$key[$i]], ENT_QUOTES);
		else if((isset($key[$i]))&&isset($row_root[$key[$i]]))
            $r2["content"] = htmlspecialchars_decode($row_root[$key[$i]], ENT_QUOTES);
		
		array_push($rs, $r2);
	}

	if($row['problemsetter'] && $row['anonymous']=='') {
        $tpl->assign("has_probset", true);
        $tpl->assign("problemsetter", htmlspecialchars_decode($row['problemsetter']));
	}
	mysql_close($con);
    $tpl->assign("rs", $rs);
    $tpl->assign("msg", $message);
	if(!isset($_GET['view']))
		$tpl->display("base.html");
	else
		$tpl->display("base2.html");
?>
