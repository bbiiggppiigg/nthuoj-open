<?php
	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

	if( !check_coowner() )
        die("You have no judge permission");

	$errors = array();
	$message = '';

	if(isset($_POST['submit'])) {
        $con = get_database_object();

		$message = check_pid($_POST['pid']);
		if($message == '') {
			if(!check_pname($_POST['pname']))
				$message = 'Problem name must contain 1-64 characters.';
			else {
				$pname = htmlspecialchars($_POST['pname'],ENT_QUOTES);
				$query = "SELECT pid FROM problems WHERE pid = ".$_POST['pid'];
				$result = mysql_query($query) or die('query failed'.mysql_error());
				if(mysql_num_rows($result) == 0) {
					$query = "INSERT INTO problems (pid,pname,memory_limit,time_limit,special_judge,cid,gid) VALUES (".$_POST['pid'].",'".$pname."',32,10,'',0,".$_POST['gid'].")";
					mysql_query($query) or die('query failed'.mysql_error());
					mysql_close($con);
					header('Location: edit_problem.php?pid='.$_POST['pid']);
					exit;
				} else {
					$message = 'Problem ID already exists.';
                }
			}
		}
		mysql_close($con);
	}

    $tpl = new Handler("Creating Problem", "create_problem.tpl");
    $tpl->assign("msg", $message);
    $tpl->display("base.html");
?>
