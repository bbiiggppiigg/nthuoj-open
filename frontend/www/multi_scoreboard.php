<?php
	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

	$errors = array();
	$message = '';
    
    $tpl = new Handler("Multi Contest Archive", "multi_contest.tpl");

    $con = get_database_object();	
	$tmp = get_latest_contest($con);
	$cur_t = time();
	$query = "SELECT cid, cname, start_time, end_time, result 
              FROM contest 
			  ORDER BY start_time DESC";
	$result = mysql_query($query) or die("Query failed".mysql_error());
	
    $rs = array();
    while($row = mysql_fetch_array($result,MYSQL_ASSOC)) {
		if(time() < strtotime($row['start_time']))	continue;
		if(time() > strtotime($row['end_time']) ) continue;
        if($row['cid'] == 1)    continue;
        array_push($rs, $row);
	}
    //$rs_time_reverse = array_reverse($rs);
    $tpl->assign("contest", $rs);
	mysql_close($con);

    $tpl->display("base.html");
?>
