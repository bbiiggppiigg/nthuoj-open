<?php
/********************************************
login.php
This gives a login page.
POST parameter 'btnForget' is checked to relocate to 'forgetPassword.php'
POST parameter 'btnLogin' is checked to login.
If 'btnLogin' is checked, POST parameter 'txtUserId', 'txtPassword' is used as user ID and password to login.
********************************************/
	session_start();
    include_once("lib/base.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

	$errors = array();
	$message = '';

  if( isset($_POST['btnForget']) ) {
    header('Location: ./forgetPassword.php');
    exit();
  }
  $tpl = new Handler('Login', 'login.tpl');
	if( isset($_POST['btnLogin']) ) {
		$rules = array();
		require("validation.php");
		$rules[] = "required,txtUserId,User ID is required.";
		$rules[] = "required,txtPassword,Password is required.";
		$rules[] = "length=3-13,txtUserId,Invalid User ID.";
		$rules[] = "length=4-13,txtPassword,Invalid Password.";
		$rules[] = "is_alpha,txtUserId,Invalid User ID.";
		$rules[] = "is_alpha,txtPassword,Invalid Password.";
		
		$errors = validateFields($_POST,$rules);
		if(!empty($errors)) {
			$message = $errors[0];
		} else {
            $con = get_database_object();
			
			$userId = $_POST['txtUserId'];
			$password = $_POST['txtPassword'];

			$query = "SELECT id FROM users WHERE id = '$userId' AND password = PASSWORD('$password')";
			$result = mysql_query($query) or die('Query failed.' . mysql_error());

			if(mysql_num_rows($result) == 1) {
				$tmp = mysql_fetch_array($result,MYSQL_ASSOC);

				$_SESSION['db_is_logged_in'] = true;
				$_SESSION['uid'] = $tmp['id'];
				$query = "select id from admin where id = '".$tmp['id']."'"; 
				echo $query;
				$res = mysql_query($query) or die("GG".mysql_error());
				if($ans = mysql_fetch_assoc($res)){
					echo "isAdmin";
					$_SESSION['isAdmin'] =true;
				}else{
					$query = "select id from judge where id = '".$tmp['id']."'"; 
					echo $query;
					$res = mysql_query($query) or die("GG".mysql_error());
					if($ans = mysql_fetch_assoc($res)){
						$_SESSION['isJudge'] =true;
					}else{
						$query = "select id from coowner where id = '".$tmp['id']."'"; 
						$res = mysql_query($query) or die("GG".mysql_error());
						if($ans = mysql_fetch_assoc($res)){
							$_SESSION['isCoowner'] =true;
						}
					}
				}
				
				$datetime = date("Y-m-d/H:i:s");
				$fp = fopen("../log.txt","a");
				fwrite($fp,$datetime."\t".$userId."\t".$_SERVER['REMOTE_ADDR']."\n");
                fclose($fp);
				header('Location: submit.php');
				exit;
			} else {
				$message = 'Invalid User ID / Password';
            }
		}
	}
    $tpl->assign("msg", $message);
    $tpl->display("base.html");
?>
