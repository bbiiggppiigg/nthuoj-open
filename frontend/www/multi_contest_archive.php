<?php  
/***********************************************
multi_contest_archive.php
This page shows all contests competing right now.
No parameter is required.
***********************************************/
 
    session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
 
    $errors = array();
    $message = '';
 
    $tpl = new Handler("Multi Contest Archive", "multi_contest.tpl");
        $con = get_database_object();
        
    $rs = getRunningContest();
    
    $tpl->assign("contest", $rs);
    $tpl->display("base.html");
?>