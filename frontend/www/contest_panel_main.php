<?php
/**************************************
contest_panel_main.php
This renders a page that shows a all contests and could add/del or update contests.
Check GET parameter 'del' to delete contest.
Check POST parameter 'submit' to add new contest.
***************************************/

	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
	if( !check_coowner() )
        die("You don't have judge permission");
	$isadmin =check_adminis();
	$errors = array();
	$message = '';
	$uid = $_SESSION['uid'];
    $tpl = new Handler("Contest Admin Panel", "contest_panel_main.tpl");

    $con = get_database_object();
	//Delete 
	if(isset($_GET['del']) && $_GET['del'] != 1) {
		$query = "DELETE FROM pid_cid WHERE cid = ".$_GET['del'];
		mysql_query($query) or die('query failed'.mysql_error());
		$query = "DELETE FROM contest WHERE cid = ".$_GET['del'];
		mysql_query($query) or die('query failed'.mysql_error());
        $message = "Delete Successfully";
		//header('Location: contest_panel_main.php');
		//exit;
	}
	//Add a new contest
	if(isset($_POST['submit'])) {
		if(!check_pname($_POST['cname']))
			$message = 'Contest name must contain 1-64 characters.';
		else {
			$cname = htmlspecialchars($_POST['cname'],ENT_QUOTES);
			$query = "INSERT INTO contest (cname,owner) VALUES ('".$cname."','".$_SESSION['uid']."')";
			mysql_query($query) or die('query failed'.mysql_error());
            $message = "Add Successful";
		}
	}

	
	if(check_adminis()){
		
		$query = "SELECT cid,cname,start_time,end_time,owner FROM contest ORDER BY cid DESC";
	}else if(check_judge() ){//Non-Administrator judge can only see their own contests
		
		$query = "SELECT cid,cname,start_time,end_time,owner FROM contest WHERE owner = '{$uid}' ORDER BY cid DESC";
	}else{
		//$query = "SELECT cid,cname,start_time,end_time,owner FROM contest WHERE owner LIKE '{$_SESSION['uid']}' ORDER BY cid DESC";
		$query = "SELECT ct.cid,cname,start_time,end_time,owner FROM contest ct WHERE ct.cid in (select C.cid from contest_coowner C where id = '{$uid}') ORDER BY ct.cid DESC";
	}
	$result = mysql_query($query) or die("Query failed".mysql_error());
    $rs = array();
    while($row = mysql_fetch_assoc($result)){
        array_push($rs, $row);
	}
    
		
    $tpl->assign("msg", $message);
    $tpl->assign("rs", $rs);
    $tpl->assign("uid",$uid);
	$tpl->assign("isadmin",$isadmin);
	$tpl->display("base.html");
?>
