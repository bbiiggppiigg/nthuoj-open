<?php
	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

	$errors = array();
	$message = '';

    $tpl = new Handler("FAQ", "faq.tpl");
    $con = get_database_object();

	if(isset($_GET['del'])) {
		$query = "DELETE FROM faq WHERE qid = ".$_GET['del'];
		mysql_query($query) or die('query failed'.mysql_error());
		mysql_close($con);
        header('Location: faq.php');		
		exit;
	}

	if(isset($_POST['submit'])) {
		if(strlen($_POST['qid'])==0){
			$message = "No QID";
		}
		if(strlen($_POST['ques'])==0){
			$message = "No Question";
		}
		if(strlen($_POST['ans'])==0){
			$message = "No Answer";
		}
		if($message == '') {
			$query = "SELECT qid FROM faq WHERE qid = ".$_POST['qid'];
			$result = mysql_query($query) or die('1query failed'.mysql_error());
			if(mysql_num_rows($result) == 0) {
				$questionText = htmlspecialchars($_POST['ques'],ENT_QUOTES);
				$answerText = htmlspecialchars($_POST['ans'],ENT_QUOTES);
				$query = "INSERT INTO faq (qid) 
						  VALUES (".$_POST['qid'].")";
				mysql_query($query) or die('query failed'.mysql_error().$query);
				$query = "UPDATE faq SET question='".$_POST['ques']."'".","."answer='".$_POST['ans']."' WHERE qid = ".$_POST['qid'];
				mysql_query($query) or die('query failed'.mysql_error().$query);
				mysql_close($con);
				header('Location: faq.php');
				exit;
			} else {
				$message = 'Question ID already exists.';
			}
			
		}
		
	}

	$query = "SELECT qid,question,answer FROM faq ORDER BY qid";
	$result = mysql_query($query) or die("Query failed".mysql_error());

    $rs = array();
	while($row = mysql_fetch_array($result, MYSQL_ASSOC))
        array_push($rs, $row);
    $tpl->assign("rs", $rs);
    $tpl->assign("msg", $message);
    
	mysql_close($con);
    $tpl->display("base.html");
?>
