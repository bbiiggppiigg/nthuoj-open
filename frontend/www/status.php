<?php
/*********************************
status.php 
Render page that shows the status of submissions.
without any GET parameter --> all submissions
with 'pid' --> get submission for the problem with same pid
with 'uid' --> get the user's submmision
with 'page' --> get the submissions should be show in the page
**********************************/

	session_start();
	header('Refresh: 90');
	include_once("lib/base.php");
	include_once("lib/contest_lib.php");
	include_once("lib/status_lib.php");
	include_once("lib/database_tools.php");
	include_once("lib/handler.php");

    $is_pid_set=0;
	$is_uid_set=0;
	$current_cid = get_latest_contest();
	$con = get_database_object();

	$query = "SELECT start_time, end_time, freeze, result 
              FROM contest 
              WHERE cid = ".$current_cid;
	$result = mysql_query($query) or die("Query failed.");
	$row = mysql_fetch_array($result, MYSQL_ASSOC);

	$start_time = $row['start_time'];
	$end_time = $row['end_time'];
	$freeze = $row['freeze'];
	$result = $row['result'];
	$message="";
    if(!check_login())
		$current_user="";
	else
		$current_user=$_SESSION['uid'];

    // check if the pid is valid
	if(isset($_GET['pid'])) {
		$pid = $_GET['pid'];
		if (!ctype_digit($pid))
			broken_page("Bad request.\n");

		if (check_login() && check_admin()){
			// query for admin user
			$query = "SELECT pid, pname
					FROM problems
					WHERE pid = $pid
				";
		}
		else{
			// query for non-admin users
			$query = "SELECT problems.pid, problems.pname
                  FROM problems
                  INNER JOIN pid_cid ON problems.pid = pid_cid.pid
                  INNER JOIN contest ON pid_cid.cid = contest.cid
                  WHERE (contest.cid = 1 OR NOW() >= contest.start_time)
                    AND problems.pid = $pid
               ";
		}
		($result = mysql_query($query, $con)) or die("Query failed.\n");
		if (mysql_num_rows($result) == 0)
			broken_page("Problem ID does not exist.");

		$row = mysql_fetch_array($result, MYSQL_ASSOC);
		$pname = $row["pname"];
		$is_pid_set = true;
	}

	$rowsPerPage = 20;
	$PageNum = 1;
	if(isset($_GET['page']))
		$PageNum = $_GET['page'];

	$is_showing_details = 0;
	$offset = ($PageNum - 1) * $rowsPerPage;
	
	
	if(isset($_GET['uid']) && $current_user==$_GET['uid']){
		$is_uid_set = 1;
		$uid = $_GET['uid'];
		$query = "SELECT sid, date, uid, submissions.pid, problems.pname, status, cpu, memory, source
                FROM submissions
                INNER JOIN problems ON problems.pid = submissions.pid
				INNER JOIN users ON users.id = submissions.uid
				WHERE uid = '$uid' ";
		($result = mysql_query($query, $con)) or die("Query failed.\n");
		// user can always see his/her code in "my submission" page
			$is_showing_details = 2;
	}
	else if(!check_login()) {
	  // normal user or un-login user
		$nowTime = time();
		if ($nowTime >= strtotime($start_time) && $nowTime < strtotime($end_time)) {
			$query = "
                SELECT sid, date, uid, submissions.pid, problems.pname, status, cpu, memory, source
                FROM submissions
                INNER JOIN problems ON problems.pid = submissions.pid
				INNER JOIN users ON users.id = submissions.uid
				WHERE ";


			if ( isset($pid) )
				$query .= " submissions.pid = $pid AND ";

			$query .= "(users.id = '$current_user'
				OR (
					submissions.date >= '$start_time'
                	AND submissions.date < DATE_SUB('$end_time', INTERVAL $freeze MINUTE)
                	AND users.user_level < 7
				))
				";

			// hide submission details during the contest
			$is_showing_details = 0;
		} else {
			$query = "
				SELECT sid, date, uid, submissions.pid, problems.pname, status, cpu, memory, source
				FROM submissions
				INNER JOIN problems ON problems.pid = submissions.pid
				INNER JOIN users ON users.id = submissions.uid
				WHERE users.user_level < 7 
				";

			if( isset($pid) )
				$query .= " AND submissions.pid = $pid";

			// check if the user is able to view the submission details when contests not running
			$is_showing_details = check_advusr() ? 2 : 1;
		}
	} else {	// judge can view all submissions
		$query = "
				SELECT sid, date, uid, submissions.pid, problems.pname, status, cpu, memory, source
				FROM submissions
                INNER JOIN problems ON problems.pid = submissions.pid
		        ";
        if( isset($pid) )
            $query .= " WHERE submissions.pid = $pid";
  

        // judge can always view the details
        $is_showing_details = 2;
	}
	$query .= "
				ORDER BY sid DESC
			";
			
	($result = mysql_query($query)) or die('Error, query failed'.mysql_error());
	$numrows = mysql_num_rows($result);
	$maxPage = ceil($numrows/$rowsPerPage);
	
	$query = $query." LIMIT $offset, $rowsPerPage";
	($result = mysql_query($query)) or die('Error, query failed'.mysql_error());

	if(!check_login() || !check_admin()) {
		if(time() >= strtotime($end_time) - $freeze * 60 && time() < strtotime($end_time) )
			echo 'You can only see your submssions during the period the scoreboard is <font color = "red">FREEZED</font>.';
	}

	$rs = array();
	$detail = array();
	while($row = mysql_fetch_assoc($result)) {
        $row["color"] = getStatusColor($row["status"]);
        $row["sourceone"] = ($row["source"] == "c") ? "C" : "C++";
        $row["view_src"] = ($is_showing_details == 2 || ($is_showing_details == 1 && $current_user == $row["uid"]));
        
		$row['status'] = showStatus($row['status']);
		
		$query = "SELECT pid, tid, verdict, runtime, memoryAmt, errMsg 
					FROM submission_result_detail
					WHERE sid = " .$row['sid']. "
					ORDER BY tid ASC
					";
		$result2 = mysql_query($query) or die('Query Failed '.mysql_error());
		$detail = array();
		while($row2 = mysql_fetch_assoc($result2)){
			$row2['runtime'] = sprintf("%.3f", $row2['runtime']/1000);
			array_push($detail, $row2);
		}
		$row["detail"] = $detail;
		array_push($rs, $row);
	}
	
	
	$tpl = new Handler("Status", "status.tpl");

	$self = $_SERVER['PHP_SELF'];
	if($PageNum > 1) {
		$page = $PageNum - 1;
        $pid_str = "";
        $uid_str = "";
		if ($is_pid_set)
            $pid_str = "&pid=$pid";
		if($is_uid_set)
			$uid_str = "&uid=$uid";
		$tpl->assign("prev_page"," <a href=\"$self?page=$page$pid_str$uid_str\"> [Prev] </a>");
		$tpl->assign("first_page"," <a href=\"$self?page=1$pid_str$uid_str\"> [First Page] </a>");
	} else {
		$tpl->assign("prev_page", " [Prev] ");
		$tpl->assign("first_page"," [First Page] ");
	}

	if($PageNum < $maxPage) {
		$page = $PageNum + 1;
        $pid_str = "";
        $uid_str = "";
		if ($is_pid_set)
            $pid_str = "&pid=$pid";
		if($is_uid_set)
			$uid_str = "&uid=$uid";
		$tpl->assign("next_page", " <a href=\"$self?page=$page$pid_str$uid_str\"> [Next] </a>");
		$tpl->assign("last_page", " <a href=\"$self?page=$maxPage$pid_str$uid_str\"> [Last Page] </a>");
	} else {
		$tpl->assign("next_page", " [Next] ");
		$tpl->assign("last_page", " [Last Page] ");
	}
	mysql_close($con);

	//$tpl->assign("is_showing_details", $is_showing_details);
	$tpl->assign("msg", $message);
	$tpl->assign("rs", $rs);
	$tpl->assign("detail", $detail);
	$tpl->assign("is_pid_set", $is_pid_set);
	if ($is_pid_set) {
      $tpl->assign("pid", $pid);
      $tpl->assign("pname", $pname);
	}
	$tpl->display("base.html");
?>
<?php
 function broken_page($err_msg = "") {
    $tpl = new Handler("Broken Page", "broken_page.tpl");
    $tpl->assign("displayed_msg", $err_msg);
    $tpl->display("base.html");
    exit(0);
 }
?>
