<?php
/*************************
 * user_panel.php
 * This page shows all user accounts registrated in this OJ system.
 * No parameter is required.
 * *********************/
	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

	if( !check_admin() )
        die("You have no judge account");

	$errors = array();
	$message = '';

    $tpl = new Handler("User Panel", "user_panel.tpl");
	
    $con = get_database_object();
	$query = "SELECT id,email,user_level,nickname FROM users ORDER BY user_level";
	$result = mysql_query($query) or die("Query failed".mysql_error());
    $rs = array();
    
	while($row = mysql_fetch_array($result, MYSQL_ASSOC))
        array_push($rs, $row);
	mysql_close($con);

    $tpl->assign("rs", $rs);
    $tpl->assign("msg", $message);
    $tpl->display("base.html");
?>
