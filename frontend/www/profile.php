<?php
/*****************************
 profile.php
 This page shows the user profile of the one loggin in.
 POST parameter 'btnSubmit' and 'password' is used to update password.
 * **************************/
	session_start();
	$errors = array();
	$message = "";
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

    $tpl = new Handler("Profile", "profile.tpl");

    if(!check_login()) {
		header('Location: login.php');		
		exit;
	}
    $con = get_database_object();
	
	if(isset($_POST["btnSubmit"])) {
		require_once("validation.php");
		$rules = array();
	
		/* require all */
		$rules[] = "same_as,password,confirm_password,Please ensure the passwords you enter are the same.";
		$rules[] = "required,email,Email is required.";
		$rules[] = "required,nickname,Nickname is required.";
		
		$rules[] = "if:password!=,length=4-12,password,Password must have length of 4-12.";
		$rules[] = "length<=12,nickname,Nickname must have length not greater than 12.";
		
		$rules[] = "is_alpha,password,Password must only consist of only (0-9 a-Z).";

		$rules[] = "valid_email,email,Email is invalid.";
		$errors = validateFields($_POST, $rules);

		if(!empty($errors))
			$message = $errors[0];
		else {
			if($_POST['password']!='') {
				$query = "UPDATE users SET password=PASSWORD('".$_POST['password']."') WHERE id = '".$_SESSION['uid']."'";
				mysql_query($query) or die('Query failed.'.mysql_error());
			}
			$query = "UPDATE users SET email='".$_POST['email']."', nickname='".$_POST['nickname']."' WHERE id = '".$_SESSION['uid']."'";
			$message = 'Update successful.';
			mysql_query($query) or die('Query failed.'.mysql_error());
		}
	}

	$query = "SELECT * FROM users WHERE id = '".$_SESSION['uid']."'";
	$result = mysql_query($query) or die("Query failed".mysql_error());
	$fields = mysql_fetch_array($result, MYSQL_ASSOC);
	if(!$fields)
		die("No such uid");
	mysql_close($con);

    $tpl->assign("uid", $fields['id']);
    $tpl->assign("email", $fields['email']);
    $tpl->assign("nickname", $fields['nickname']);
    $tpl->assign("msg", $message);

    $tpl->display("base.html");
?>
