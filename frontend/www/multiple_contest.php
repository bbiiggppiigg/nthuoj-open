<?
	ob_start();
	session_start();
    header("Refresh: 90");

    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
	
    $tpl = new Handler("Contest", "multiple_contest.tpl");

    $tmp = get_latest_contest();
	if(isset($_GET["cid"])) {
		if($_GET["cid"] && ctype_digit($_GET["cid"]) && $_GET["cid"]!=1)
			$current_cid = $_GET["cid"];
		else
			die("no such contest.");
	} else {
		$current_cid = $tmp;
    }
	
	if($current_cid != -1) {
        $con = get_database_object();

		$query = "SELECT cname,start_time,end_time,freeze FROM contest WHERE cid = ".$current_cid;
		$result = mysql_query($query) or die(mysql_error()."Query failed1.");
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		if(!$row)
			die("no contest. orz...");
	
		$cname = $row['cname'];
		$start_time = $row['start_time'];
		$end_time = $row['end_time'];

		if( time() < strtotime($row['start_time'])
            && !check_admin() ) {
			echo "This content is unavailable<br />";
			header('./contest_archive.php');
			exit;
		}

        $tpl->assign("is_running", true);
		$query = "SELECT cname,start_time,end_time,freeze 
                  FROM contest 
                  WHERE cid = ".get_latest_contest($con);
		$result = mysql_query($query) or die(mysql_error()."Query failed1.");
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		if(time() < strtotime($row['start_time']) || time() >= strtotime($row['end_time']) ) {
		
			$query = "SELECT start_time, cname 
                      FROM contest 
                      WHERE start_time > NOW() ORDER BY start_time ASC LIMIT 1";
			$result = mysql_query($query) or die("Query failed".mysql_error());
			$row = mysql_fetch_assoc($result);
			if($row) {
				if(strtotime($row['start_time'])-time() < 48*60*60)
					$message = 'The next contest \''.$row['cname'].'\' will be started in '.toperiod(strtotime($row['start_time'])-time());
			}

            $tpl->assign("is_running", false);
		}
	}
	
    if($current_cid == -1)	die('no contest');
		
    $query = "SELECT problems.pid,pname FROM problems 
              INNER JOIN pid_cid on problems.pid = pid_cid.pid 
              WHERE problems.pid=problems.gid AND pid_cid.cid = ".$current_cid." 
              ORDER BY problems.pid";
	$result = mysql_query($query) or die("Query failed".mysql_error());

    $rs = array();
	while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $row["valuetext"] = htmlspecialchars_decode($row["pname"], ENT_QUOTES);
        array_push($rs, $row);
    }
	mysql_close($con);

    $result = get_clarification($_SESSION['uid'], $current_cid);
    $clarification = array();
    while( $row = mysql_fetch_array($result, MYSQL_ASSOC) )
        array_push($clarification, $row);
   
    $tpl->assign("cid", $current_cid);
    $tpl->assign("clarification", $clarification);
    $tpl->assign("rs", $rs);
    $tpl->assign("contest_name", $cname);

    $tpl->display("base.html");
?>
