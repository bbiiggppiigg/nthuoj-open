<?php
/************************************************************
edit_problem.php
This renders a page for editting a problem.
Checks GET parameter 'pid' to recognize which problem to update.
Checks POST parameter 'hasSubmit' as a sign to update problem.
All update information is in other POST parameter.
Checks POST parameter: parent_pid, pname, problemsetter, tid, anonymous.
If one of those parameter exists, problem would be update.
Checks FILE parameter: judge_program, jin to update file.
***************************************************************/


	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
	include_once("lib/problem_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
    include_once("fckeditor/fckeditor.php");
    
	
	if( !check_admin() )
        die("You have no judge permission");

    $tpl = new Handler("Edit Problem", "edit_problem.tpl");

	$message = '';
    $con = get_database_object();

	if(!isset($_GET["pid"]))
	if(!isset($_GET["pid"]))
		die('Problem ID does not exist~');

    $pid = $_GET['pid'];
    if(!ctype_digit($pid))
		die('Problem ID does not exist!');

	$query = "SELECT pid FROM problems WHERE pid =".$pid;
	$result = mysql_query($query) or die("Query failed".mysql_error());
	if(mysql_num_rows($result)==0)
		die('Problem ID does not exist@');

    $tpl->assign("pid", $pid);
	
	$sBasePath = './fckeditor/';
	$editor = array();
	$key = array();
	$title = array();
	$key[0] = 'description';
	$key[1] = 'input';
	$key[2] = 'output';
	$key[3] = 'sample_input';
	$key[4] = 'sample_output';

	$title[0] = 'Problem Description';
	$title[1] = 'Input';
	$title[2] = 'Output';
	$title[3] = 'Sample Input';
	$title[4] = 'Sample Output';
	$page_link = '';

	if(isset($_POST['submit'])) {
		$page_link = $_POST['page_link'];
		$query = "UPDATE problems SET ";
		for($i = 0; $i <= 4; $i++) {
			if($i == 0 && $page_link != '' && $_POST[$key[$i]] == '') {
				if( get_magic_quotes_gpc() )
					$postedValue = htmlspecialchars( stripslashes(file_get_contents($page_link)), ENT_QUOTES);
				else
					$postedValue = htmlspecialchars( file_get_contents($page_link), ENT_QUOTES);
			} else {
				if( get_magic_quotes_gpc() )
					$postedValue = htmlspecialchars( stripslashes( $_POST[$key[$i] ] ),ENT_QUOTES );
				else
					$postedValue = htmlspecialchars( $_POST[$key[$i]], ENT_QUOTES ) ;
			}
			if($i!=0)
				$query = $query.",";
			$query = $query.$key[$i]."='".$postedValue."'";
		}
		$query = $query." WHERE pid = ".$pid;
		mysql_query($query) or die("Query failed ".mysql_error());

        $query = "SELECT * FROM problems WHERE pid=".$pid;
        $result = mysql_query($query) or die("Query failed ".mysql_error());
        $row = mysql_fetch_row($result);
        if($row['tid']!='0'){
            $query = "UPDATE mapping SET realid='".$_POST['rid']."' where pid=".$pid;
            mysql_query($query) or die("Query failed ".mysql_error());
        }

		$message = 'Problem updated.';
		
		if(!ctype_digit($_POST['parent_pid']) ){
			$message = $message." Invalid parent pid.";
		}
		else {
			$root_pid = getRootPid($_POST['parent_pid']);
			$query = "UPDATE problems SET parent_pid= ".$root_pid." 
                      WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed ".mysql_error());
			$query = "SELECT * FROM problems WHERE pid = ".$root_pid;
			$result = mysql_query($query) or die("Query failed :".mysql_error().$query);
			$row = mysql_fetch_array($result,MYSQL_ASSOC);
		}
		
		
		if(!$_POST['pname'] && !checkIsRootPid($pid) ){
			/* if no pname, get it from root problem*/
			$query = "UPDATE problems SET pname='".htmlspecialchars($row['pname'],ENT_QUOTES)."' 
                      WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed ".mysql_error());
		}
		else if(!check_pname($_POST['pname']))
			$message = $message." Invalid problem name.";
		else {
			$query = "UPDATE problems SET pname='".htmlspecialchars($_POST['pname'],ENT_QUOTES)."' 
                      WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed ".mysql_error());
		}
		

		if( !ctype_digit($_POST['tl']) || $_POST['tl'] < 1 || $_POST['tl'] > 60)
			$message = $message." Time limit must be an integer between 1 and 60.";
		else {
			$query = "UPDATE problems SET time_limit=".$_POST['tl']." 
                      WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed ".mysql_error());
		}

		if( !ctype_digit($_POST['ml']) || $_POST['ml'] < 1 || $_POST['ml'] > 256)
			$message = $message." Memory limit must be an integer between 1 and 256.";
		else {
			$query = "UPDATE problems SET memory_limit=".$_POST['ml']." 
                      WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed ".mysql_error());
		}
		
		if(!check_pbname($_POST['problemsetter']))
			$message = $message." Invalid problemsetter name.";
		else {
			$query = "UPDATE problems SET problemsetter='".htmlspecialchars($_POST['problemsetter'],ENT_QUOTES)."' 
                      WHERE pid = ".$pid;
			mysql_query($query) or die("Query failed ".mysql_error());
		}
		
		$query = "UPDATE problems SET anonymous='".$_POST['anonymous']."' 
                  WHERE pid = ".$pid;
		mysql_query($query) or die("Query failed ".mysql_error());

		$query = "UPDATE problems SET special_judge='".$_POST['special_judge']."' 
                  WHERE pid = ".$pid;
		mysql_query($query) or die("Query failed ".mysql_error());
		
		if($_FILES["if"]['name']) {
			if ($_FILES["if"]["error"] > 0)
				$message = $message."Input file uploading error.";
			elseif ($_FILES["if"]["size"] > 33554432)
				$message = $message."Input file too large. (limit: 32MB)";
			else {
				if($out_path = check_uncompress($_FILES["if"]["name"],$_FILES["if"]["tmp_name"])) {
					$path = "../nthuoj/testdata/".$pid.".in";
					convert_upload($path,$out_path);
					if(file_exists($out_path))
						unlink($out_path);
				} else {
					$message = $message."The compressed file must contain exactly one input file.";
                }
			}
		}
		
        if($_FILES["of"]['name']) {
			if ($_FILES["of"]["error"] > 0)
				$message = $message."Output file uploading error.";
			elseif ($_FILES["of"]["size"] > 33554432)
				$message = $message."Output file too large. (limit: 32MB)";
			else {
				if($out_path = check_uncompress($_FILES["of"]["name"],$_FILES["of"]["tmp_name"])) {
					$path = "../nthuoj/testdata/".$pid.".out";
					convert_upload($path,$out_path);
					if(file_exists($out_path))
						unlink($out_path);
				} else {
					$message = $message."The compressed file must contain exactly one output file.";
                }
			}
		}

		if($_FILES["judge_program"]['name']) {
			if ($_FILES["judge_program"]["error"] > 0)
				$message = $message."Judge program uploading error.";
			elseif ($_FILES["judge_program"]["size"] > 10240)
				$message = $message."Judge program too large. (limit: 10KB)";
			else {
				if($out_path = check_uncompress($_FILES["judge_program"]["name"],$_FILES["judge_program"]["tmp_name"])) {
					$query = "SELECT special_judge FROM problems WHERE pid = ".$pid;
					$result = mysql_query($query) or die("Query failed ".mysql_error());
					$row = mysql_fetch_assoc($result);
					if(!$row)
						die("orz");
					$path = "../nthuoj/testdata/".$pid.".".$row['special_judge'];
					convert_upload($path,$out_path);
					if(file_exists($out_path))
						unlink($out_path);
				} else {
					$message = $message."The compressed file must contain exactly one judge program file.";
                }
			}
		}

		if($_FILES["jin"]['name']) {
			if ($_FILES["jin"]["error"] > 0)
				$message = $message."Judge input file uploading error.";
			else if ($_FILES["jin"]["size"] > 33554432)
				$message = $message."Judge input file too large. (limit: 32MB)";
			else {
				if($out_path = check_uncompress($_FILES["jin"]["name"],$_FILES["jin"]["tmp_name"])) {
					$path = "../nthuoj/testdata/".$pid.".jin";
					convert_upload($path,$out_path);
					if(file_exists($out_path))
						unlink($out_path);
				} else {
					$message = $message."The compressed file must contain exactly one judge input file.";
                }
			}
		}
	}
	
    $query = "SELECT * FROM problems WHERE pid =".$pid;
	$result = mysql_query($query) or die("Query failed".mysql_error());
	$row = mysql_fetch_array($result, MYSQL_ASSOC); 
    $query = "SELECT * FROM mapping WHERE pid =".$pid;
    $realid ="";
    if( $row['tid']!='0' ) {
	    $result = mysql_query($query) or die("Query failed".mysql_error());
        $row2 = mysql_fetch_array($result);
        $realid = $row2['realid'];
    }

    $tpl->assign("realid", $realid);
    $tpl->assign("pname", $row['pname']);
	$tpl->assign("parent_pid", $row['parent_pid']);
    $tpl->assign("time_limit", $row['time_limit']);
    $tpl->assign("memory_limit", $row['memory_limit']);
    $tpl->assign("problemsetter", $row['problemsetter']);
    $tpl->assign("anonymous", $row['anonymous']);
    $tpl->assign("page_link", $page_link);

	$path = "../nthuoj/testdata/".$pid.".in";
	if(file_exists($path) )
		$input_link = '<a target = "_blank" href="testdata.php?pid='.$pid.'.in">'.$pid.'.in</a>';
	else
		$input_link = 'Input file does not exist.';

	$path = "../nthuoj/testdata/".$pid.".out";
	if(file_exists($path) )
		$output_link = '<a target="_blank" href="testdata.php?pid='.$pid.'.out">'.$pid.'.out</a>';
	else
		$output_link = 'Output file does not exist.';
	
    if($row['special_judge']!="") {
		if($row['special_judge'] == "c")
			$ftype = "c";
		else if($row['special_judge'] == "cpp")
			$ftype = "cpp";
		else
			die("wrong");

		$path = "../nthuoj/testdata/".$pid.".".$ftype;
		if(file_exists($path) )
			$jdg_link = '<a target = "_blank" href="testdata.php?pid='.$pid.'.'.$ftype.'">'.$pid.'.'.$ftype.'</a>';
		else
			$jdg_link = 'Judge program does not exist.';
	} else {
		$jdg_link = 'Judge program does not exist.';
    }
	
    $path = "../nthuoj/testdata/".$pid.".jin";
	if(file_exists($path) )
		$jdg_input = '<a target="_blank" href="testdata.php?pid='.$pid.'.jin">'.$pid.'.jin</a>';
	else
		$jdg_input = 'Judge input file does not exist.';

    $tpl->assign("input_link", $input_link);
    $tpl->assign("output_link", $output_link);
    $tpl->assign("jdg", $row['special_judge']);
    $tpl->assign("jdg_link", $jdg_link);
    $tpl->assign("jdg_input", $jdg_input);
    
    $rs = array();
	for($i = 0; $i <= 4; $i++) {
		$editor[$i] = new FCKeditor($key[$i]);
		$editor[$i]->Config["CustomConfigurationsPath"] = "/fckeditor/myconfig.js";
		$editor[$i]->Width = "99%";
		$editor[$i]->BasePath = $sBasePath;
		$editor[$i]->Value = htmlspecialchars_decode($row[$key[$i]], ENT_QUOTES);
        $row['title'] = $title[$i];
		$row['content'] = $editor[$i]->CreateHtml();
        array_push($rs, $row);
	}
    mysql_close($con);

    $tpl->assign("msg", $message);
    $tpl->assign("rs", $rs);
    $tpl->display("base.html");
?>
