/*****************************************************************************
scoreboard.php :
This php renders a page of scoreboard with rank and penalty of a contest.
'cid' should be given as 'get' parameter.
ex. scoreboard.php?cid=100 shows the scoreboard of contest with cid 100.
********************************************************************************/

<?php
	ob_start();
	session_start();
	header('Refresh: 20');
    include_once("lib/base.php");`
    include_once("lib/contest_lib.php");
    include_once("lib/status_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/scoreboard_tools.php");
    include_once("lib/handler.php");
	$message= "";
	$all = array();
    
    $tpl = new Handler("Scoreboard_Basic", "scoreboard.tpl");

	/*check cid try : Get or Session or kick you away!!*/
	if(isset($_GET["cid"])) {
		if($_GET["cid"] && ctype_digit($_GET["cid"]) && $_GET["cid"]!=1){
			$current_cid = $_GET["cid"];
			$_SESSION["cid"] = $current_cid;
		}
		else
			die("no such contest.");
	} 
	else if(isset($_SESSION["cid"]) ){
		$current_cid = $_SESSION["cid"];
	}
	else {
		go_multi_contest_page();
    }
	
	
	/* if there's a contest, get info*/
	if($current_cid != -1) {
        $con = get_database_object();

		$query = "SELECT cname, start_time, end_time, freeze, result 
                  FROM contest 
                  WHERE cid = ".$current_cid;
		$result = mysql_query($query) or die("Query failed1.");
		$row = mysql_fetch_array($result, MYSQL_ASSOC);
		if(!$row)   die("no contest");

		if( time() < strtotime($row['start_time']) ) {
			echo "This contest is not available.";
			exit;
		}

		$cname = $row['cname'];
		$start_time = $row['start_time'];
		$end_time = $row['end_time'];
		$freeze = $row['freeze'];
		$res = $row['result'];

        $tpl->assign("contest_name", $cname);
        $tpl->assign("freeze", $freeze);
        $tpl->assign("res", $res);
        $tpl->assign("freeze_period", toperiod($freeze * 60));
	}

	if($current_cid==-1)
		die('no contest');


	$first_time = time() - strtotime($start_time);
	$second_time = strtotime($end_time) - time();
    $tpl->assign("first_time", $first_time);
    $tpl->assign("second_time", $second_time);


	if( $first_time >= 0 && $second_time > 0) {
		$elapsed = toperiod(time() - strtotime($start_time));
		$remain = toperiod(strtotime($end_time) - time());
        $tpl->assign("elapsed", $elapsed);
        $tpl->assign("remain", $remain);
	}
	
	$query = "SELECT problems.pid, pname 
              FROM problems 
              INNER JOIN pid_cid ON problems.pid = pid_cid.pid 
              WHERE pid_cid.cid = ".$current_cid." 
              ORDER BY problems.pid";
	$result = mysql_query($query) or die("Query failed.");
	$pname = array();
    $p_name = array();
	while($row = mysql_fetch_array($result, MYSQL_ASSOC) ) {
		$pname[$row['pid']] = $row['pname'];
        array_push($p_name, $row['pid']);
    }
    $tpl->assign("pname", $p_name);

	/*get all submissions*/
    if( (!check_login() || !check_coowner() )
        && (time() >= strtotime($end_time) - $freeze * 60)
        && ($res == "no" || time() < strtotime($end_time))
      ) {
	  /* for freeze scoreboard */
		$query = "SELECT sid, submissions.pid, uid, status, date 
                  FROM submissions 
                  INNER JOIN problems ON submissions.pid = problems.pid 
                  INNER JOIN pid_cid ON problems.pid=pid_cid.pid 
                  WHERE pid_cid.cid = ".$current_cid." 
                    AND submissions.date >= '".$start_time."' 
                    AND submissions.date < DATE_SUB('".$end_time."',INTERVAL ".$freeze." MINUTE) 
                    AND uid NOT LIKE '%judge%'
                  ORDER BY submissions.date ASC";
	} 
	else if( !check_login() || !check_coowner() ){
		/* un-freezed scoreboard for ordinary user*/
		$query = "SELECT sid, submissions.pid, uid, status, date 
                  FROM submissions 
                  INNER JOIN problems ON submissions.pid = problems.pid 
                  INNER JOIN pid_cid ON problems.pid=pid_cid.pid 
                  WHERE pid_cid.cid = ".$current_cid." 
                    AND submissions.date >= '".$start_time."' 
                    AND submissions.date < '".$end_time."' 
					AND uid NOT LIKE '%judge%'
                  ORDER BY submissions.date ASC"; 
	}
	else {
		/*scoreboard for judges*/
		$query = "SELECT sid, submissions.pid, uid, status, date 
                  FROM submissions 
                  INNER JOIN problems ON submissions.pid = problems.pid 
                  INNER JOIN pid_cid ON problems.pid=pid_cid.pid 
                  WHERE pid_cid.cid = ".$current_cid." 
                    AND submissions.date >= '".$start_time."' 
                    AND submissions.date < '".$end_time."' 
                  ORDER BY submissions.date ASC"; 
    }
	
	/* put submission info into $all*/
	($result = mysql_query($query)) or die("Query failed.");
	$all = array();
    $rs = array();
	while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		if( passNewProblem($all, $row) )
			$all[$row['uid']][$row['pid']]['accepted'] = true;
		$all[$row['uid']]['uid'] = $row['uid'];
		$all[$row['uid']][$row['pid']]['status'] = $row['status'];
	}
	foreach($all as $key => $value) {
		$all[$key]['solved'] = 0;
		$all[$key]['penalty'] = 0;
		foreach($pname as $key2 => $value2) {
			if(isset($all[$key][$key2]['accepted'])  
				&& $all[$key][$key2]['accepted'] ){
				$all[$key]['solved']++;
				$all[$key]['penalty'] += getPenalty($key, $key2, $start_time, $end_time);
			}
		}
	}
	
	usort($all,"cmp");

	$ranking = 1;
	$real_ranking = 1;
	$last_solved = -1;

	foreach($pname as $key2 => $value2)
		$total_solve[$key2] = 0;
	/*deal with result to show: score & solved*/
	
	foreach($all as $key => $value) {
        $row = array();
        $row['rank'] = $ranking;
		
        $row['uid'] = $all[$key]['uid'];
        
        $row['user'] = array();
        $id = 0;
		foreach($pname as $key2 => $value2) {
			if(isset($all[$key][$key2]['status']))
				$row['user'][$id] = getHighestStatus($row['uid'], $key2, $start_time, $end_time);
			else
				$row['user'][$id] = "--";
			if(isset($all[$key][$key2]['accepted'])
			 && $all[$key][$key2]['accepted'] ) {
				$total_solve[$key2]++;
			}
            $id ++;
		}
        $row['solved'] = $all[$key]['solved'];
        $row['penalty'] = $all[$key]['penalty'];
        array_push($rs, $row);
		$ranking ++;
	}
    
    $solved_count = array();
	foreach($pname as $key2 => $value2)
        array_push($solved_count, $total_solve[$key2]);
    $tpl->assign("sv", $solved_count);
    $tpl->assign("rs", $rs);
    $tpl->assign("msg", $message);
	mysql_close($con);

    $tpl->display("base.html");
?>
