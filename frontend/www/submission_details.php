<?php
/**********************************************
submission_detail.php
This Renders a page that shows the submitted code.
Checks the GET parameter 'sid' to show a certain submission's code.
**********************************************/


    session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

    function broken_page($err_msg = "")
    {
        $tpl = new Handler("Broken Page", "broken_page.tpl");
        $tpl->assign("displayed_msg", $err_msg);
        $tpl->display("base.html");
        exit(0);
    }

    $current_cid = get_latest_contest();
    $con = get_database_object();

    if (!check_login())
      $current_user = "";
    else
      $current_user = $_SESSION["uid"];

    $query = "SELECT start_time, end_time
              FROM contest
              WHERE cid = $current_cid
             ";
    ($result = mysql_query($query, $con)) or die("Query failed.");
    $row = mysql_fetch_array($result);
    $start_time = $row["start_time"];
    $end_time = $row["end_time"];
    $now_time = time();

    // if not logged in
    if (!check_login())
        broken_page("You are not authorized to view this source.\n");

    // if the contest is running, only the judges are able to view sources
    if (!check_admin() && ($now_time >= strtotime($start_time) && $now_time < strtotime($end_time)))
        broken_page("Submission details are hidden during the contest.\n");

    if (!isset($_GET["sid"]))
        broken_page("Bad request.\n");

    $sid = $_GET["sid"];
    if (!ctype_digit($sid))
        broken_page("Bad request.\n");

    if (check_advusr()) {
      // the viewer's user level must be greater than or equal to the submitter's
      $query = "SELECT sid, date, uid, submissions.pid, problems.pname, status, cpu, source
                FROM submissions
                INNER JOIN problems ON problems.pid = submissions.pid
                INNER JOIN users ON users.id = submissions.uid
                WHERE (sid = $sid
                  AND (" . $_SESSION['user_level'] . " + 1) >= users.user_level)
                  ";
    } else {
      $query = "SELECT sid, date, uid, submissions.pid, problems.pname, status, cpu, source
                FROM submissions
                INNER JOIN problems ON problems.pid = submissions.pid
                INNER JOIN users ON users.id = submissions.uid
                WHERE sid = $sid";
				/*WHERE (sid = $sid
                  AND (" . $_SESSION['user_level'] . " + 1) >= users.user_level
                  AND '" . $_SESSION['uid'] . "' LIKE submissions.uid)
               ";*/
    }
    ($result = mysql_query($query, $con)) or die("Query failed.");
    $row = mysql_fetch_array($result);

    if (!$row)
        broken_page("The user is not found,
                     or you are not authorized to view this source.\n");

	/*if($row['source']==".c")
		$full_path = "../nthuoj/source/$sid.cpp";
    $full_path = "../nthuoj/source/$sid.c";*/
	$full_path = "../nthuoj/source/$sid.".$row['source'];
	if (!file_exists($full_path)){
		broken_page("A Source not found.\n$sid.".$row['source']);
	}else if(!is_file($full_path)){
			broken_page("B Source not found.\n$sid.".$row['source']);
	}

    $submission = $row;
    $submission["color"] = get_status_color($submission["status"]);
    $tpl = new Handler("Submission Details", "submission_details.tpl");
    $code = htmlspecialchars(file_get_contents($full_path));
    $tpl->assign("code", $code);
    $tpl->assign("submission", $submission);
    $tpl->display("base.html");

    exit(0);
?>

