<?php
/********************************
logout.php
This is used to logout.
Session of the user is killed.
No parameter is required.
********************************/

	session_start();
	if(isset($_SESSION['db_is_logged_in']))
		unset($_SESSION['db_is_logged_in']);
	if(isset($_SESSION['user_level']))
        unset($_SESSION['user_level']);
    session_destroy();
	header('Location: index.php');
?>
