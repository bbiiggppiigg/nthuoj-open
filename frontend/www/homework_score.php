<?php
/*************************************
 * homework.php
 * This gives a page for courses' homework use.
 * If POST parameter 'sm' is checked, uploads FILE 'problems' and 'students' as input.
 * All passed students would be printed.
 * ***********************************/

	session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
	include_once("lib/problem_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");
	
	$prob_path = "homework_problist.txt";
	$stud_path = "homework_studlist.txt";
	if( !check_admin() )
        die("You have no judge permission");
    if( isset($_POST["sm"])){

	    move_uploaded_file($_FILES['problems']['tmp_name'] , $prob_path);
	    move_uploaded_file($_FILES["students"]["tmp_name"], $stud_path);
        $pp=$_FILES['problems']['tmp_name'];
        $ss=$_FILES['students']['tmp_name'];

        $prob_file=fopen($prob_path,"r");
        fscanf($prob_file, "%s", $homework_name);
        $start_time = "0000-00-00T00:00";
        fscanf($prob_file, "%s", $end_time);
        fscanf($prob_file, "%d", $n_prob);
        $prob_score = array();
        for($i=0; $i<$n_prob; $i++){
            fscanf($prob_file, "%d %f", $pid, $pscore);
            $prob_score[$pid] = $pscore;
        }
        fclose($prob_file);

        $stud_file=@fopen($stud_path,"r");
        $n_stud = 0;
        $stud_id = array();// OJID -> studentID
        
        while (($line = fgets($stud_file, 4096)) !== false) {
            if($line==' ')continue;
            if(sscanf($line, "%d%s%s", $stuid, $name, $ojid)!=3)continue;
            //echo "@$line<br>";
            $stud_id[$ojid] = $stuid;
            $n_stud++;
        }
        
        fclose($stud_file);
        $con = get_database_object();

		$query = "SELECT * 
				  FROM submissions
				  WHERE date >= '".$start_time."'".
				  "AND date < '".$end_time."'"; 
        ($result = mysql_query($query)) or die("Query failed.");

        $user_ac = array();

		while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			if(passProblem($row)){
				//echo $row['sid']." ".$row['date']." ".$row['uid']." ".$row['pid']." "."<br>";
				$uid=$row['uid'];
				$pid=$row['pid'];
				if(isset($stud_id[$uid]) && isset($prob_score[$pid])){
					$user_ac[$uid][$pid] = 1;
					//print $uid." AC ".$pid."<br>";
				}
			}
		}
        print "<div class=\"container\">";
		//array_multisort($stud_id);
        echo $homework_name."<br>";
        echo "#problems: ".$n_prob."<br>";
        echo "#students: ".$n_stud."<br>";
        echo "deadline: ".$end_time."<br>";
        print "studentID\tNTHUOJ ID\tscore\t#solved\tsolved"."<br>";
		foreach ($stud_id as $ojid => $stuid) {
			print $stuid."\t".$ojid."\t";
			$score=0.0;
			$prob_solved="";
            $num_solved=0;
			if(isset($user_ac[$ojid])){
				foreach ($user_ac[$ojid] as $pid => $value) {
					$prob_solved .= $pid.",";
					$score+= $prob_score[$pid];
                    $num_solved++;
				}
			}
			print "\t".$score."\t".$num_solved."\t".$prob_solved."<br>";
		}
        print "</div>";
    }

    $tpl = new Handler("Homework Score", "homework_score.tpl");
    $tpl->display("base.html");
?>


<?php
    function passNewProblem($all, $row)
    {   
        if(isset( $all[$row['uid']][$row['pid']]['accepted']) 
         && $all[$row['uid']][$row['pid']]['accepted']==true )
            return false; /*already passed*/
        else if( passProblem($row) )
            return true; 
        else
            return false;
    }
    function passProblem($row)
    {
        $str = $row['status'];
        $a = strtok($str, "/");
        $b = strtok("/");
        if( $a===$b )
            return true;
        else return false;
    }
?>