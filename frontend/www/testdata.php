<?
/**********************
 * testdata.php
 * A link that downloads a submitted code or testdata.
 * If GET paramter 'pid' is checked, a testdata of the problem would be downloaded.
 * IF GET parameter 'src' is checked, submitted code would be downloaded.
 * *******************/
	session_start();
    include_once("lib/base.php");
    include_once("lib/database_tools.php");

	$message = '';

	if(isset($_GET["pid"]) && $_GET["pid"]) {
        if(!check_login() || !check_admin() ) {
			header("HTTP/1.1 403 Forbidden");	
			die("Forbidden");
			exit;
		}

		$path = $_GET["pid"];
		$full_path = "../nthuoj/testdata/".$path;
		if(!$path)
			die("File does not exist.");	
		if(!file_exists($full_path) || !is_file($full_path) )
			die("File does not exist.");
	} elseif(isset($_GET["src"]) && $_GET["src"] ) {
		$pos = strrpos($_GET["src"],".");
		if($pos === false)	die("Forbidden");

		$sid = substr($_GET["src"],0,$pos);

		if(!is_numeric($sid) )	die("Forbidden");

        $con = get_database_object();

		$query = "SELECT sid,uid FROM submissions WHERE sid = ".$sid;
		($result = mysql_query($query)) or die("Query failed1.");
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		if(!$row)	die("Forbidden");
		if(!check_login() || !check_coowner()) {
			header("HTTP/1.1 403 Forbidden");	
			die("Forbidden");
			exit;
		}
		$path = $_GET["src"];
		$full_path = "../nthuoj/source/".$path;
		if(!$path)
			die("File does not exist.");	
		if(!file_exists($full_path) || !is_file($full_path) )
			die("File does not exist.");
	} else {
		die("Forbidden");
    }
	header('Cache-control: private');
	header('Content-Type: application/octet-stream');
	header('Content-Length: '.filesize($full_path));
	header('Content-Disposition: filename ='.$path);
	flush();

	$fd = fopen($full_path,"r");
	while(!feof($fd)) {
		$buffer = fread($fd, 2048);
		echo $buffer;
	}
	flush();
?>
