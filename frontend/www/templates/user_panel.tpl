<h3>User Panel</h3>
<div align="center">
<table class="table table-striped">
  <tr>
    <th width="20%">ID</th>
    <th>Email</th>
    <th>User Level</th>
    <th>Nickname</th>
  </tr>

  <{section name=id loop=$rs }>
    <tr class="test" name="<{ $rs[id].id }>">
      <td><{ $rs[id].id }></td>
      <td><{ $rs[id].email }></td>
      <td><{ $rs[id].user_level }></td>
      <td><{ $rs[id].nickname }></td>
    </tr>
  <{/section}>
</table>
</div>
