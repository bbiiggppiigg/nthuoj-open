<h3>Personal Statistics</h3>
<div>
<table style="width: 270px;" class="table table-striped table-bordered">
  <{section name=id loop=$rs1 }>
    <tr>
      <{if $rs1[id].status}>
	  <th width="200px"><{$rs1[id].status}></th>
      <td align="right"><{$rs1[id].cnt}></td>
	  <{/if}>
    </tr>
  <{/section}>
  <th>Total submissions</th>
  <td align="right"><{$total}></td>
</table>

<table class="table table-striped">
  <tr>
    <th>Problem ID</th>
    <th>Problem Title</th>
  </tr>  
  <{section name=id loop=$rs_prob }>
    <tr>
      <td><a href=./problem.php?pid=<{$rs_prob[id].pid}>><{$rs_prob[id].pid}></a></td>
      <td><a href=./problem.php?pid=<{$rs_prob[id].pid}>><{$rs_prob[id].pname}></a></td>
    </tr>
  <{/section}>
</table>
</div>
