<h3>Submit Problem</h3>
<form method="post" enctype="multipart/form-data">
  <div align="center">
    <table class="table table-striped" >
      <tr>
        <td>
          <h4>Problem ID: &nbsp;
          <input type="text" size="4" name="pid" value="<{ $pid }>" /></h4>
        </td>
        <td align=right><h4>
          <input type="radio" value="c" name="lang"/> C <br>
          <input type="radio" value="cpp" name="lang" checked/> C++
        </h4></td>
      </tr>
      
      <tr>
        <td colspan=2>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;
          <textarea rows="15" cols="50" name="code" style="width:75%;align:center;"></textarea>
        </td>
      </tr>

      <tr>
        <td>
          <!--<label for="upfile"><b>Filename:</b></label>-->
          <input type="file" name="upfile" id="upfile" />
        </td>
        <td align=right>
          <input type="submit" name="sm" value="Submit" class="btn btn-primary">
        </td>
      </tr>
    </table>
  </div>
</form>
<{if $timelock!=''}>
	<script>alert("上傳間隔過短，上傳不受理");</script>
	<script>alert("理性的使用系統，不但可以降低系統的負擔，也可以使眾人得到更好的服務");</script>
<{/if}>
