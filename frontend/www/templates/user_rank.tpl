﻿<h3>User Rank</h3>
<div align="center">
<table class="table table-striped">
  <tr>
    <th>Rank</th>
    <th>User ID</th>
    <th>Solved Problems</th>
  </tr>
  <{section name=id loop=$rs }>
    <tr>
      <th>
	    <{ $rs[id].rank }> 
		<{if ($rs[id].pic == 0 ) }>
			<img src = "pic/champion.png" width = 16 height = 16>
		<{elseif ($rs[id].pic == 1 ) }>	
			<img src = "pic/diamond.png" width = 16 height = 16>			
		<{elseif ($rs[id].pic == 2 ) }>
			<img src = "pic/gold.png" width = 16 height = 16>
		<{elseif ($rs[id].pic == 3 ) }>
			<img src = "pic/silver.png" width = 16 height = 16>
		<{elseif ($rs[id].pic == 4 ) }>
			<img src = "pic/bronze.png" width = 16 height = 16>
		<{/if}>
		
	  </th>
      <td><{ $rs[id].id }></td>
      <td><{ $rs[id].cnt }></td>
    </tr>
  <{/section}>
</table>
