<{if $cid neq 1 }>
  <h3>Contest Panel</h3>
  <form method="post" name="contest_attr">
  <table class="table table-striped">
    <tr>
      <th>Contest Name</th>
      <th>Start Time</th>
      <th>Show Result</th>
    </tr>
    <tr>
      <td><input name="cname" type="text" size=16 maxlength=64 value="<{ $cname }>" ></td>
      <td><input name="start_time" type="text" size=32 maxlength=64 value="<{ $start_time }>" ></td>
      <td>
        <input type="radio" value="yes" name="result" <{if $res }> checked="yes" <{/if}> />yes<br>
        <input type="radio" value="no" name="result" <{if $res eq false }> checked="yes" <{/if}> />no</td>
    </tr>
    <tr>
      <th>Freeze Scoreboard(mins)</th>
      <th>End Time</th>
      <th>&nbsp;</th>
    </tr>
    <tr>
      <td><input name="freeze" type="text" size=8 maxlength=8 value="<{ $freeze }>"></td>
      <td><input name="end_time" type="text" size=32 maxlength=64 value="<{ $end_time }>"></td>
      <td><input type="submit" name="update" value="Update" class="btn btn-primary"></td>
    </tr>
  </table>
  </form>
<{else}>
  <h2>General Problems</h2>
<{/if}>
<form method="post" name="frmLogin">
<table class="table table-striped">
  <tr>
    <th>Problem ID</th>
    <td>&nbsp;</td>
    <td><input name="pid" type="text" size=4 maxlength=4></td>
    <td><input name="submit" type="submit" value="Add Problem" class="btn btn-primary"></td>
  </tr>
</table>
</form>

<table class="table table-striped">
  <tr>
    <th width="20%">Problem ID</th>
    <th>Problem Name</th>
    &nbsp;
    <th></th>
  </tr>
  <{section name=id loop=$rs }>
    <tr>
      <td><{ $rs[id].pid }></td>
      <td><a href=problem.php?pid=<{ $rs[id].pid }> ><{ $rs[id].pname }></a></td>
      <td><a href=contest_panel_sub.php?cid=<{ $cid }>&pid=<{ $rs[id].pid }> onClick="return confirm('Are you sure that you want to DELETE this problem from the contest?'">[delete]</a></td>
    </tr>
  <{/section}>
</table>
<br><br><br><br><br>
<br><br><br><br><br>
