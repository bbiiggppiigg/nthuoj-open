<h3>Contest Archive</h3>
<table class="table table-striped">
  <tr>
    <th width="20%">Contest ID</th>
    <th>Contest Name</th>
    <th>Start Time</th>
    <th>End Time</th>
  </tr>

  <{section name=id loop=$contest }>
    <tr>
      <td><{ $contest[id].cid }></td>
      <td>
        <a href=contest.php?cid=<{ $contest[id].cid }> ><{ $contest[id].cname }></a>
        <{if ($contest[id].result eq "yes") }>
          [<a href=scoreboard.php?cid=<{ $contest[id].cid }> >Result</a>]
        <{/if}>
      </td>
      <td><{ $contest[id].start_time }></td>
      <td><{ $contest[id].end_time }></td>
    </tr>
  <{/section}>
</table>
