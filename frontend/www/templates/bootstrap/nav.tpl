<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container-fluid">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <a class="brand" href="./index.php"><img src="./pic/TitlebarLogo2.png" width="150"></a>
      <div class="nav-collapse collapse">
        <ul class="nav">
          <{if $is_login }>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Personal<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="./my_submission.php">Submission</a></li>
                <li><a href="./my_statistics.php">Statistics</a></li>
                <li><a href="./profile.php">Profile</a></li>
              </ul>
            </li>
          <{/if}>

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contest<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="./contest_archive.php">Archive</a></li>
              <li><a href="./contest.php">Problem Set</a></li>
              <li><a href="./scoreboard.php">Scoreboard</a></li>
              <!-- <li><a href="./new_scoreboard.php">Scoreboard V2</a></li> -->
            </ul>
          </li>


          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Volume<b class="caret"></b></a>

            <ul class="dropdown-menu">

              <li><a href="./volume.php?vol=1">Volume 1</a></li>

              <li><a href="./volume.php?vol=2">Volume 2</a></li>

              <li><a href="./volume.php?vol=3">Volume 3</a></li>

              <li><a href="./volume.php?vol=4">Volume 4</a></li>

              <li><a href="./volume.php?vol=5">Volume 5</a></li>

              <li><a href="./volume.php?vol=6">Volume 6</a></li>

              <li><a href="./volume.php?vol=7">Volume 7</a></li>

              <li><a href="./volume.php?vol=8">Volume 8</a></li>

<!--              <li><a href="./volume.php?vol=9">Volume 9</a></li>-->

            </ul>

          </li>



          <li class="dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">CPE<b class="caret"></b></a>

            <ul class="dropdown-menu">

              <li><a href="./contest.php?cid=71">Sample Problems</a></li>

              <li class="dropdown-submenu">

                <a tabindex="-1" href="#">101學年度</a>

                <ul class="dropdown-menu">

                  <li><a href="./contest.php?cid=148">上學期第一次</a></li>

                  <li><a href="./contest.php?cid=149">上學期第二次</a></li>

                  <li><a href="./contest.php?cid=150">上學期第三次</a></li>

                </ul>

              </li>

              <li class="dropdown-submenu">

                <a tabindex="-1" href="#">100學年度</a>

                <ul class="dropdown-menu">

                  <li><a href="./contest.php?cid=86">上學期第一次</a></li>

                  <li><a href="./contest.php?cid=103">上學期第二次</a></li>

                  <li><a href="./contest.php?cid=107">上學期第三次</a></li>

                  <li><a href="./contest.php?cid=112">下學期第一次</a></li>

                  <li><a href="./contest.php?cid=117">下學期第二次</a></li>

                  <li><a href="./contest.php?cid=123">下學期第三次</a></li>

                </ul>

              </li>

              <!--<li><a href="#">Contest</a></li>-->

            </ul>

          </li>



          <li class="dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Course<b class="caret"></b></a>

            <ul class="dropdown-menu">

              <li><a href="./course_info.php">課程資訊</a></li>
			  <li><a href="./faq.php">都市傳說</a></li>
			  <li><a href="./basic_class.php">基礎班課綱</a></li>
			  <li><a href="./contest.php?cid=159">基礎班題目</a></li>
			  <li><a href="./basic_classHW.php">基礎班解題情形</a></li>
			  <li><a href="./tpc2013spring.php">TPC 2013 Spring</a></li>

			  <!--li><a href="#">中階班解題情形</a></li-->

            </ul>

          </li>



          <li class="dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tools<b class="caret"></b></a>

            <ul class="dropdown-menu">

              <li><a href="http://www.codeblocks.org/">Codeblocks</a></li>

              <li><a href="http://notepad-plus-plus.org/">Notepad++</a></li>

              <li><a href="http://www.mingw.org/">MinGW</a></li>
            </ul>
          </li>

          <{if $is_admin }>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admins<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="./contest_panel_main.php">Contest Panel</a></li>
              <li><a href="./problem_panel.php">Problem Panel</a></li>
              <li><a href="./user_panel.php">Users Panel</a></li>
              <li><a href="./format_transfer.php">Format Transformation</a></li>
              <li><a href="./log_info.php">Log Info</a></li>
			  <li><a href="./rejudge.php">Rejudge</a></li>	   
			  <li><a href="./findSid.php">Find Sid</a></li>
            </ul>
          </li>
          <{/if}>

          <li><a href="./submit.php">Submit</a></li>
          <li><a href="./status.php">Status</a></li>
          <!--li><a href="./april_fool_status.php">Status</a></li-->
          <li><a href="./user_rank.php">User Rank</a></li>
          <!--<li class="active"><a href="https://docs.google.com/spreadsheet/viewform?formkey=dDFXczRTbFhiRXRmeG54QUlKMi1BWlE6MQ">Bug Report</a></li>-->

          <!--<li><a href="#contact">Contact</a></li>-->
        </ul>

        <{if not $is_login }>
          <button type="submit" class="btn pull-right" onClick="window.location='./register.php'">Register</button>
          <form class="navbar-form pull-right" method="post" action="./login.php">
            <input class="span1" type="text" name="txtUserId" placeholder="Account">
            <input class="span1" type="password" name="txtPassword" placeholder="Passwd">
            <button type="submit" class="btn" name="btnLogin">Sign in</button>&nbsp;
          </form>
        <{else}>
          <button type="submit" class="btn pull-right" onClick="window.location='./logout.php'">Logout</button>
        <{/if}>
      </div><!--/.nav-collapse -->
    </div>
  </div>
</div>
