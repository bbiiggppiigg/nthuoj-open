<h3>Log Info</h3>
<div>
<table class="btn btn-striped">
  <tr>
    <th>Remote IP</th>
    <th>User ID</th>
    <th>Date</th>
  </tr>

  <{section name=id loop=$rs }>
    <tr>
      <td><{ $rs[id].ip }></td>
      <td><{ $rs[id].uid }></td>
      <td><{ $rs[id].date }></td>
    </tr>
  <{/section}>
</table>
</div>

