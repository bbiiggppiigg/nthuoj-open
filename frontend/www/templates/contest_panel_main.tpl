<h3>Contest Panel</h3>
<form method="post" name="frmLogin">
<table class="table">
  <tr>
    <th>Contest Name</th>
    <td><input name="cname" type="text" size=32 maxlength=64 ></td>
    <td><input type="submit" name="submit" value="Add Contest" class="btn btn-primary"></td>
  </tr>
</table>
</form>

<table class="table table-striped">
  <tr>
    <th width="20%">Contest ID</th>
    <th>Contest Name</th>
    <th>Start Time</th>
    <th>End Time</th>
    <th>Contest Owner</th>
	<th>&nbsp;</th>
  </tr>

  <{section name=id loop=$rs }>
    <tr>
      <td><{ $rs[id].cid }></td>
      <td><a href=contest_panel_sub.php?cid=<{ $rs[id].cid }> ><{ $rs[id].cname }></a></td>
      <td><{ $rs[id].start_time }></td>
      <td><{ $rs[id].end_time }></td>
      <td><{ $rs[id].owner }></td>
	  <td>
	  <{if $rs[id].cid neq 1 }>
		<{if ($rs[id].owner eq $uid) || ($isadmin) }>
        <button class="btn"><a href=contest_panel_main.php?del=<{ $rs[id].cid }> onClick="return confirm('Are you sure that you want to DELETE this contest?')" ><b>Delete</b></a></button>
		<{/if}>
		</td>
	  <{/if}>
    </tr>
  <{/section}>
</table>
