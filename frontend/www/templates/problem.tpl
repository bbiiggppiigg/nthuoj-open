
<table class="table table-striped">
<tr>
  <td>
    <h2>
      <{ $pid }> - <{ $pname }> 
      <{if $is_admin }>
         [<a href=edit_problem.php?pid=<{ $pid }> >Edit</a>]
      <{/if}>
    </h2>
  </td>
  <td>
  <div align="right">
    <h5><a href="status.php?pid=<{ $pid }>" >Status</a> &nbsp;|&nbsp; <a href="problem_print.php?pid=<{ $pid }>" >Printable </a> &nbsp;|&nbsp; <a href=rank.php?pid=<{ $pid }> >Rank</a> &nbsp;|&nbsp; <a href=submit.php?pid=<{ $pid }> >Submit</a></h5>
  <hr noshade size="2px" style="color:#FFFFFF;">
  <h5>
    Parent PID: <{ $parent_pid }> <br>
	<{ if $special_judge eq true }>
    Judge with Validator<br>
    <{ /if }>
	Limits: [<a style="color:blue;"  onclick="showLimits();" href="#">see</a>]
  
  </h5>
  </div>
  </td>
</tr>
</table>

<hr>
<br>
<{section name=id loop=$rs }>
  <div>
    <h3><{ $rs[id].title }></h3>
    <{ $rs[id].content }>
  </div>
<{/section}>

<hr noshade size="1px" color=#606070>

<{if $has_probset }>
  <h5>Problemsetter: <{ $problemsetter }></h5>
<{/if}>

<div id="limits"  style="display:none">
	Testcase: <br><br><br>
	<{section name=id loop=$time_limit }>
	  <div>
		
			<{$smarty.section.id.iteration}> :
			&nbsp Time limit : <{ $time_limit[id] }> S, &nbsp  
			Memory limit : <{ $memory_limit[id] }> MB
		
	  </div>
	  <br>
	<{/section}>
</div>
<div id="backAll" style="display:none;" onclick="hideLimits();"></div>
<script>
	function showLimits(){
		var el = document.getElementById("limits");
		el.style.display = "";
		var el = document.getElementById("backAll");
		el.style.display = "";
	}
	function hideLimits(){
		var el = document.getElementById("limits");
		el.style.display = "none";
		var el = document.getElementById("backAll");
		el.style.display = "none";
	}
</script>

<style>
#limits{
	color: #F0F0F0 ;
	position:fixed; 
	top:25%; 
	width:100%;
	overflow:auto;
	z-index:10;
	font-size:1.5em;
	text-align:center;
}
#backAll{
	background-color: rgba(10%, 15%, 10%, 0.8);
	position:fixed;
	top:0;
	left:0;
	width:100%;
	height:100%;
	z-index:0;
}
</style>