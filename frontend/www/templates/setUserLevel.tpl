
<table class = "table table-striped">
<th>Change the User Level of a current user to Contest Coowner</th>
<tr>
	<form method="post" name="coowner">
	<td>
		<select name="setAsCoowner">
		<{section name=id loop=$user}>
		　<option value="<{$user[id].id}>"><{$user[id].id}></option>
		<{/section}>
	</select>
	</td>
	<td>
		<input type="submit" class = "btn btn-primary" value ="set as coowner" name = "coowner" />
	</td>	
	</form>

</tr>
<th>Change the User Level of a Contest Coowner to Judge</th>
<tr>

	<form method="post" name="judge">
	<td>
		<select name="setAsJudge">
		<{section name=id loop=$coowner}>
		　<option value="<{$coowner[id].id}>"><{$coowner[id].id}></option>
		<{/section}>
		</select>
		</td>
	<td>
		<input type="submit" class = "btn btn-primary" value ="set as judge" name = "judge" /> 
	</td>
	</form>

</tr>
<th>
	Change the User Level of a Judge to Admin
</th>
<tr>

	
	<form method="post" name="admin">
	<td>
	<select name="setAsAdmin">
	<{section name=id loop=$judge}>
	　<option value="<{$judge[id].id}>"><{$judge[id].id}></option>
	<{/section}>
	</select>
	</td>
	<td>
		<input type="submit" class = "btn btn-primary" value ="set as admin" name="admin" /> 
	</td>
	</form>
</tr>
</table>