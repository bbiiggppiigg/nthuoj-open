<h2><{ $contest_name }></h2>
<div align="center">
<table class="table table-striped">
    <{if $res eq "yes" }>
      <tr>
        <td>Elapsed Time: <{$elapsed}> </td>
        <td>Remaing Time: <{$remain}> </td>
      </tr>
    <{else}>
      <tr>
        <td><h4>Contest is over</h4></td>
      </tr>
    <{/if}>

    <{if ($contest_over eq false) and $freeze gt 0 }>
      <{if $is_admin }>
        <{if ($second_time gt 0) and ($second_time gt $freeze*60) }>
          <tr>
            <td colspan="2">The scoreboard will be freezed <b><{$freeze_period}></b> from the end of contest.</td></tr>
        <{elseif ($second_time gt 0) or ($res eq "no") }>
          <tr>
            <td colspan="2">The scoreboard is <font color="red"> ALREADY FREEZED </font><b><{$freeze_period}></b> from the end of contest.</td>
          </tr>
        <{/if}>
      <{elseif $second_time gt 0 }>
        <{if $second_time gt $freeze*60}>
          <tr>
            <td colspan="2">The scoreboard will be freezed <b><{$freeze_period}></b> from the end of contest for others, but admins.</td>
          </tr>
        <{elseif ($second_time gt 0) or $res eq "no"}>
          <tr>
            <td colspan="2">The scoreboard is freezed <b><{$freeze_period}></b> from the end of contest for others, but admins.</td>
          </tr>
        <{/if}>
      <{/if}>
    <{/if}>
</table>

<table style="text-align:right;" class="table table-striped">
  <tr>
    <th>Rank</th>
    <th>User</th>
    <{section name=id loop=$pname }>
      <th><{$pname[id]}></th>
    <{/section}>
    <th>Solved</th>
    <th>Penalty</th>
  </tr>

  <{section name=id loop=$rs }>
    <tr>
      <th><{$rs[id].rank}></th>
      <td><{$rs[id].uid}></td>
      <{section name=jd loop=$rs[id].user }>
        <td><{$rs[id].user[jd]}></td>
      <{/section}>
      <td><{$rs[id].solved}></td>
      <td><{$rs[id].penalty}></td>
    </tr>
  <{/section}>
  <tr>
    <td colspan=2 align="center">Total Solved</td>
    <{section name=id loop=$sv }>
      <td><{ $sv[id] }></td>
    <{/section}>
    <td colspan=2> &nbsp; </td>
  </tr>
</table>
</div>

<div>
  <form>
    <input type='button' id='scrollButton' value='自動捲動' onclick='changeScrollMode();'>
  </form>
<script>
  var locate = -1;
  var dir = 1;
  var scrolling = 0;
  var timer;
  function scroller() {
    if(window.pageYOffset == locate)  {
      dir = -dir;
	  document.cookie="dir="+dir;
    }
    locate = window.pageYOffset;
    scrollBy(0, 2 * dir);
  }
  function changeScrollMode() {
    if ( scrolling ) {
	  document.getElementById('scrollButton').value = '自動捲動';
	  scrolling = 0;
	  clearInterval(timer);
	  document.cookie="scrollTimer=0";
	}
	else {
	  document.getElementById('scrollButton').value = '停止捲動';
	  scrolling = 1;
	  timer = setInterval("scroller()", 60);
	  document.cookie="scrollTimer="+timer;
	}
  }
  function getCookie(name) {
	var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for( var i=0 ; i < ca.length ; i++ ) {
      var c = ca[i];
      while ( c.charAt(0) == ' ' ) c = c.substring(1,c.length);
      if ( c.indexOf(nameEQ) == 0 ) return c.substring(nameEQ.length,c.length);
    }
    return null;
  }
  <!-- use cookie to check the direction and scrolling status -->
  if ( getCookie('scrollTimer') == 0 || getCookie('scrollTimer') == null ) {
    scrolling = 1;
  }
  else {
	scrolling = 0;
  }
  dir = parseInt(getCookie('dir'));
  if ( dir != 1 && dir != -1 )
	dir = 1;
  changeScrollMode();
  
</script>
</div>
<br><br><br>
