<{if $findUser eq 1 }>
<form method="post" name="findUser">
  User ID: <input name="uid" type="text" size=12 maxlength=12>&nbsp;<input type="submit" name="btnFind" value="Submit" class="btn btn-primary">
</form>
<{/if}>
<{if $is_pid_set}>
<h3>Status of <a href=problem.php?pid=<{ $pid }> ><{ $pid }> - <{ $pname }></a></h3>
<{else}>
<h3>General Status</h3>
<{/if}>
<div align="center">
<table class="table table-striped">
  <tr>
    <th width="100px">Submit ID</th>
    <th width="150px">Date</th>
    <th width="100px">Username</th>
    <th>Problem</th>
    <th width="180px">Status</th>
    <th width="50px">Source</th>
  </tr>

  <{section name=id loop=$rs }>
    <{if ($rs[id].color eq '"red"') }>
		<tr class="status success"
			onmouseover="show('<{$rs[id].sid}>_id2');" 
			onmouseout="hide('<{$rs[id].sid}>_id2');"> 
    <{elseif ($rs[id].color eq '"blue"') }>
		<tr class="status info" 
			onmouseover="show('<{$rs[id].sid}>_id2');" 
			onmouseout="hide('<{$rs[id].sid}>_id2');">
    <{elseif ($rs[id].color eq '"green"') }>
		<tr class="status error" 
			onmouseover="show('<{$rs[id].sid}>_id2');" 
			onmouseout="hide('<{$rs[id].sid}>_id2');">
    <{else}>
		<tr class="status warning" 
		onmouseover="show('<{$rs[id].sid}>_id2');" 
		onmouseout="hide('<{$rs[id].sid}>_id2');">
    <{/if}>
	
        <td>
        <{if $rs[id].view_src}>
            <a href=submission_details.php?sid=<{ $rs[id].sid }> ><{ $rs[id].sid }></a>
        <{else}>
            <{ $rs[id].sid }>
        <{/if}>
        </td>
      <td><{ $rs[id].date }></td>
      <td><{ $rs[id].uid }></td>
      <td><a href=problem.php?pid=<{ $rs[id].pid }> ><{ $rs[id].pid }> - <{ $rs[id].pname }></a></td>
	
      <td><b>
		<font color=<{ $rs[id].color }> > 
        <{if ($rs[id].status eq 'Compile Error') and $is_login and ($is_admin or ($suid eq $rs[id].uid)) }>
          <a href=err_msg.php?sid=<{ $rs[id].sid }> ><font color="blue"><{ $rs[id].status }></font></a>
        <{else}>
          <{ $rs[id].status }>
        <{/if}>
      </font></b></td>
      <td>
        <{if $is_login and $is_admin }>
            <a href=testdata.php?src=<{ $rs[id].sid }>.<{ $rs[id].source }> ><{ $rs[id].sourceone }></a>
        <{else}>
            <{ $rs[id].sourceone }>
        <{/if}>
      </td>    
    </tr>
  <{/section}>
</table>
</div>

<div>
	<{section name=id loop=$rs }>
		<div class="detail_bg" id="<{$rs[id].sid}>_id2" style="display:none;">
		<div class="detail_inner">
		<table>
			<tr>
				<td>
				<h4>Submit ID: &nbsp<{ $rs[id].sid }></h4> 
				</td>
			</tr>
			<tr>
				<{if $rs[id].detail[0].verdict}>
					<td>
						<h5> Result </h5>
					</td>
					<td>
						<h5> CPU </h5>
					</td>
				
				<{else}>
						  &nbsp;
				<{/if}>
			</tr>
			<{section name=id2 loop=$rs[id].detail}>
				<tr>
					<td>
						<{if $rs[id].detail[id2].verdict}>
						  <{$smarty.section.id2.iteration}> : &nbsp 
						  <{$rs[id].detail[id2].verdict}>
						<{else}>
						  &nbsp;
						<{/if}>
					</td>
					<td>
						<{if $rs[id].detail[id2].runtime}>
						  <{$rs[id].detail[id2].runtime}>
						<{else}>
						  &nbsp;
						<{/if}>
					 </td>
				</tr>
				
				<{if $rs[id].detail[id2].errMsg != "null"}>
				<tr>
					<td class="err">
						<{$rs[id].detail[id2].errMsg}>
					</td>
				</tr>
				<{/if}>
			<{/section}> 
		</table> 
		</div>
		</div>	
	<{/section}>
</div>


<p style="text-align:center"> <{ $first_page }> <{ $prev_page }> <{ $next_page }> <{ $last_page }> </p>

<script type="text/javascript">
	function show(toShow) {		
		var el = document.getElementById(toShow);
		el.style.display = "";
	}
	function hide(str) {
		var arr = document.getElementById(str);
		arr.style.display = "none";
	}
	function findPos(obj)
	{
		var curtop = 0;
		if (obj.offsetParent) {
			do {
				curtop += obj.offsetTop;
			} while (obj = obj.offsetParent);
		return [curtop];
		}
	}
</script>

<head> 
	<style type="text/css">
		.status:hover{  
			font-size:1.15em;
			font-weight:bold;
		}
		.detail_bg{
			position:fixed;
			background-color:rgba(10%, 20%, 10%, 0.8);
			color:white;
			top:10em;
			right:0;
			padding-top:0;
			-webkit-animation: fadeIn 0.5s;
			animation: fadeIn 0.5s;
		}
		.detail_inner{
			position:relative;
			top:0;
			padding-left:1.15em;
			width:100%;
		}
		.detail_inner td{
			padding-right:1em;
		}
		.err{
			width:9em;
		}
		@-webkit-keyframes fadeIn {
			from { opacity: 0; }
			  to { opacity: 1; }
		}
		@keyframes fadeIn {
			from { opacity: 0; }
			  to { opacity: 1; }
		}
	</style> 
</head>
