<{if $vol gt 1 }>
  <a class="btn pull-left" href="./volume.php?vol=<{ $vol-1 }>">Prev Volume</a>
<{/if}>
<{if $vol lt 9 }>
  <a class="btn pull-right" href="./volume.php?vol=<{ $vol+1 }>">Next Volume</a>
<{/if}>

<h3 align="center">Volume <{ $vol }></h3>
<table class="table table-striped">
  <tr>
    <th width="20%">Problem ID</th>
    <th>Title</th>
  </tr>
  <{section name=id loop=$rs }>
    <tr <{if $rs[id].accepted}> class="success" <{/if}>>
      <td><{ $rs[id].pid }></td>
      <td><a href=problem.php?pid=<{ $rs[id].pid }> ><{ $rs[id].pname }></a></td>

    </tr>
  <{/section}>
</table>
