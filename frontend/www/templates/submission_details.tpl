<script type="text/javascript" src="js/prettify.js"></script>
<link href="css/prettify.css" rel="stylesheet" type="text/css" />

<h3>Submission Details</h3>
<div>
    <table class="table">
        <thead>
            <tr>
                <th width="100px">Submit ID</th>
                <th width="150px">Date</th>
                <th width="100px">Username</th>
                <th>Problem</th>
                <th width="180px">Status</th>
                <th width="50px">CPU</th>
                <th width="50px">Source</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><{ $submission.sid }></td>
                <td><{ $submission.date }></td>
                <td><{ $submission.uid }></td>
                <td>
                    <a href=problem.php?pid=<{ $submission.pid }> >
                        <{ $submission.pid }> - <{ $submission.pname }>
                    </a>
                </td>
                <td><font color=<{ $submission.color }> ><b><{ $submission.status }></b></font></td>
                <td><{ $submission.cpu }></td>
                <td>
                    <{ if $submission.source eq 'c' }>
                        C
                    <{ elseif $submission.source eq 'cpp' }>
                        C++
                    <{ else }>
                        N/A
                    <{ /if }>
                </td>
            </tr>
        </tbody>
    </table>
    <pre class="prettyprint lang-cpp"><{$code}></pre>
    <script type="text/javascript">prettyPrint();</script>
</div>

