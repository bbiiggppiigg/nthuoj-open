<div id="content">
	<h3>基礎班</h3>
	<h4>課程資訊</h4>
	<p style="margin-left: 20px">
		上課地點：台達107教室<br/>
		上課時間：星期一 19:00~視情況而定<br/>
		　　　　　總共15次上課，期末考前結束課程<br/>
		　　　　　12次授課(含7次小考)+3次大考<br/>
		大考日期：3/20, 4/24, 5/29<br/>
		旁聽限制：不給旁聽<br/>
		指導教授：李哲榮老師 (cherung@cs.nthu.edu.tw)<br/>
		　　　　　潘雙洪老師 (spoon@cs.nthu.edu.tw)<br/>
		助教：<a href="mailto:z0000b20021@yahoo.com.tw">鍾隆翔</a>(z0000b20021@yahoo.com.tw)<br/>
	　　　<a href="mailto:wudaiyang@gmail.com">吳岱洋</a>(wudaiyang@gmail.com)<br/>
	　　　<a href="mailto:wr1218@gmail.com">鄭以琳</a>(wr1218@gmail.com)<br/>
	　　　<a href="mailto:henryyang42@gmail.com">楊宗翰</a>(henryyang42@gmail.com)<br/>
	　　　<a href="mailto:forst.and.flame11235@gmail.com">楊易霖</a>(forst.and.flame11235@gmail.com)<br/>
	</p>
	<h4>評分標準 (總分超過100%)</h4>
	<ol>
		<li style="margin-left: 20px">第一部分(55%)：
			<ul>
				<li>總共12次作業 (大考週外，每週至少四題基礎題)</li>
				<li>會有額外加分題</li>
			</ul>
		</li>
		<li style="margin-left: 20px">第二部分(55%)：
			<ul>
				<li>小考七次, 20% (取最高五次)</li>
				<li>大考三次, 分別為 10%, 10%, 15%</li>
			</ul>
		</li>
		<li style="margin-left: 20px">Bonus：
			<ul>
				<li>過CPE加總分10分，且保證及格</li>
				<li>若有參加CPE或其他比賽，會根據情況斟酌加總分</li>
			</ul>
		</li>
		<br/>
		<p>
		不點名&不會調分
		</p>
	</ol>


	<h4>課程大綱</h4>
		<a style="margin-left: 20px" href="https://www.dropbox.com/s/qewndc6hxawg0ns/CPT%20%E5%9F%BA%E7%A4%8E%E7%8F%AD%E8%AA%B2%E7%B6%B1for%E4%BF%AE%E8%AA%B2%E5%90%8C%E5%AD%B8.pdf">連結</a>


	<br/>
	<hr>
	<br/>


	<!--
	<h3>中階班</h3>
	<h4>課程資訊</h4>

	<p style="margin-left: 20px">
	上課地點：資電128教室<br/>
	上課時間：星期一 19:00~??:??<br/>
	小考時間：待補<br/>
	課程限制：擋修程式設計<br/>
	旁聽限制：不給旁聽<br/>
	指導教授：李哲榮老師 (cherung@cs.nthu.edu.tw)<br/>
	　　　　　潘雙洪老師 (spoon@cs.nthu.edu.tw)<br/>
	助教：<a href="mailto:forst.and.flame11235@gmail.com">KerKer</a>(forst.and.flame11235@gmail.com)<br/>
	　　　<a href="mailto:tony19920430@gmail.com">小魏</a>(tony19920430@gmail.com)<br/>
	</p>

	<h4>評分標準</h4>
	<ol>
		<li style="margin-left: 20px">第一部分(30%)：
			<ul>
				<li>指定作業30分 (當周deadline, 除了教DP那一周以外, 最多2題)</li>
			</ul>
		</li>
		<li style="margin-left: 20px">第二部分(70%)：(整學期一個deadline)
			<ul>
				<li>小考六次, 15分 (一題一分, 共30題可選擇性補, 共15分)</li>
				<li>大考三次, 30分 (同基礎班算法, 40 – 30 – 20 – 10 – 10 – 20 – 30 – 40 來計算)</li>
				<li>其餘題目, 25分 (一題至少一分, 非常多題, 選擇性補)</li>
			</ul>
		</li>
		<p>
		小考分數以補滿15分為限, 因為題目大部分會較為簡單!
		其餘題目25分補超過的可以當bonus加去小考 & 期中考
		但無法加入第一部分的指定作業
		</p>
	</ol>

	<h4>課程大綱</h4>
		<a style="margin-left: 20px" href="https://www.dropbox.com/s/eu69tvo4jaq2w59/%E5%A4%A7%E7%B6%B1.docx">連結</a>
		<p style="margin-left: 20px" >
		!!除了最後一次期末考外, 所有考試(期中考,小考) 皆為3人一隊考試, 請先找好隊友, 沒隊友的我們會幫你進行分配~!!
		</p>


	<br/>
	<hr>
	<br/>
	-->

	<h3>進階班</h3>
	<h4>課程資訊</h4>
	<p style="margin-left: 20px">
	上課地點：資電R447<br/>
	上課時間：星期一 19:00<br/>
	<!-- 課程限制：修課同學以大學部同學為主，不收研究生外籍生及外校生。 -->
	<!-- 旁聽限制：不允許旁聽 -->
	</p>
	<h4>評分標準</h4>
	<p style="margin-left: 20px; ">
	很可怕不要問
	</p>
	<!--ol>
	<li>Problem List 70%
			<ul>
				<li>每次討論10題，一共會討論10~13次</li>
				<li>寫出來100題以上作業分數以滿分70分計算</li>
				<li>Deadline: 06/25</li>
			</ul>
		</li>
		<li>Contest 20% (三次個人上機比賽，每次五題，進行三小時)</li>
		<li>Discuss Participation 10%</li>
	</ol-->
	<br/><br/><br/><br/><br/><br/><br/><br/>
</div>
