
<!--<div class="hero-unit">-->
  <div class="carousel slide" id="myCarousel">
    <div class="carousel-inner">
      <div class="active item">
      <div class="hero-unit">
      <img src="./pic/TitlebarLogo.png" width="500">
      <table class="table">
        <tr>
        <!--<td rowspan=2><img src="./pic/logl2.png"></td>-->
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>
          <!--h2>NTHU Online Judge</h2>-->
          <p>This is National Tsing Hua University Online Judge, an ACM-like online judge designed for training purposes and a platform of contest. You may utilize this site by the links on the side bar and top-right corner. Each of them will redirect you to a page with the following functions.</p>
          <p>
            <{if $is_login }>
            <a class="btn btn-primary btn-large" href="./logout.php">Logout &raquo;</a>
          <{else}>
            <a class="btn btn-primary btn-large" href="./register.php">Register &raquo;</a>
            <a class="btn btn-primary btn-large" href="./login.php">Login &raquo;</a>
          <{/if}>
          </p>
        </td>
        </tr>
      </table>
      </div></div>

      <{if $msg != '' }>
        <div class="item">
        <div class="hero-unit">
          <h2><{ $msg }></h2>
          <hr>
          <p>&nbsp;Click the button and join the contest!!</p>
          <a class="btn btn-primary btn-large" href="./contest.php">Join&raquo;</a>
          <br><br><br><br>
        </div>
        </div>
      <{/if}>
      
      <div class="item"><div class="hero-unit">
        <h2>New Function: Clarification</h2>
        <hr>
        <p>&nbsp;If contest is running, you can submit clarification to judge in contest area. See below picture: <br>
        <img src="http://i.imgur.com/w5ncRkX.png">
        </p>
      </div></div>
      
      <div class="item"><div class="hero-unit">
        <h2>New User Interface, New Feeling!!</h2>
        <hr>
        <p>&nbsp;Hope every user likes it!</p>
        <br><br><br><br><br>
      </div></div>
    </div>
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
  </div>
<!--</div>-->

<div class="row">
  <div class="span3">
    <h3>Top-Right Links</h3>
    <dl>
      <dt>Register</dt>
      <dd><p>You can register for an account on this page. Currently a secret key is needed for registeration.</p></dd>
      <dt>Login</dt>
      <dd><p>You may login your account for submissions of your program.</p></dd>
      <dt>Profilie</dt>
      <dd><p>This will be shown only if you login, you can edit your personal information here.</p></dd>
    </dl>
  </div>

  <div class="span3">
    <h3>Site Index</h3>
    <dl>
      <dt>Submit</dt>
      <dd><p>After finishing your program, you can submit it for judge through this page. You need to fill in problem ID and language you used, then you can submit your code by either posting it or uploading the file.</p</dd>
      <dt>Status</dt>
      <dd><p>All the verdicts will be shown here. The status, CPU time as well as memory usage will be reported for every submission, except thos ought not to be showned during the very end of the contest, when the scoreboard is freezed.</p></dd>
      <dt>Problem Archive</dt>
      <dd><p>Here you can find the problems of this site that you can try to solve.</p></dd>
    </dl>
  </div>

  <div class="span3">
    <h3>Contest</h3>
    <dl>
      <dt>Problem Set</dt>
      <dd><p>You can browse the problems of the currently running contest, if any.</p></dd>
      <dt>Scoreboard</dt>
      <dd><p>Here you can see the ranklist and status of participants of the currently running contest.</p></dd>
      <dt>Archive</dt>
      <dd><p>Here you can see all scheduled contests including the past and upcoming ones.</p></dd>
    </dl>
  </div>

  <div class="span3">
    <h3>Admin</h3>
    <p>If you are an admin, after login, you will see an additional section.</p>
    <dl>
      <dt>Contest Panel</dt>
      <dd><p>Here you can schedule for a contest as well as modify the information and problem sets for the contest, e.g. when should the scoreboard freeze before the end of contest.</p></dd>
      <dt>Problem Panel</dt>
      <dd><p>Here you can create a new problem or edit an existing problem, editor embedded is Fckeditor.</p></dd>
    </dl>
    <p>Note that aside from these additional functions, the admins are allowed to see all the status of the submission and scoreboard during it is freezed. Besides, the problems that is not added to Problem Archive will not be shown and cannot be submitted for non-admin accounts.</p>
  </div>
</div>
<a href="#top">Top ^</a>
