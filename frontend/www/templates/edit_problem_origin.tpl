<h2><{ $pid }> - <{ $pname }> [<a href=problem.php?pid=<{ $pid }> target="_blank" >View</a>] </h2>

<table class="table table-striped">
<form method = "post" enctype = "multipart/form-data">
<tr>
  <th>Problem Name: &nbsp;</th>
  <td><input name = 'pname' type = "text" size = "32" maxlength = "64" value = "<{ $pname }>"  /><br /></td>
</tr>


<tr>
  <th>Parent PID: &nbsp;</th>
  <td><input name = 'parent_pid' type = "text" size = "32" maxlength = "64" value = "<{ $parent_pid }>"  /><br /></td>
</tr>


<tr>
  <th>Time limit: &nbsp;</th>
  <td><input name = 'tl' type = "text" size = "4" maxlength = "3" value = <{ $time_limit }> ><br /></td>
</tr>

<tr>
  <th>Memory limit: &nbsp;</th>
  <td><input name= 'ml' type = "text" size = "4" maxlength = "3" value = <{ $memory_limit }> ><br /></td>
</tr>

<tr>
  <th>Problemsetter: &nbsp;</th>
  <td><input name ='problemsetter' type ="text" size = "16" maxlength = "32" value = "<{ $problemsetter }>" /></td>
</tr>

<tr>
  <th>Anonymous: &nbsp;</th>
  <td><input name="anonymous" type = "checkbox" value = "checked" <{ $anonymous }>  > <br /></td>
</tr>

<tr>
  <th>Real ID:&nbsp;</th>
  <td><input name = 'rid' type = "text" size = "4" maxlength = "10" value = <{ $realid }> ><br /></td>
  </tr>

<tr>
  <th>Problem Page:&nbsp;</th>
  <td><input name='page_link' type="text" size="10" maxlength="1000" value= <{ $page_link }> ><br /></td>
</tr>

<tr>
  <th><b>Input file: <{ $input_link }></b></th>
  <td><input type = "file" name="if" size="16" /> (RAR is acceptable in case the files are too large.)<br /></td>
</tr>

<tr>
  <th><b>Output file: <{ $output_link }></b></th>
  <td><input type= "file" name="of" size="16" /> <br /></td>
</tr>

<tr>
  <th>Special Judge:</th>
  <td>
    <input type = "radio" value = "" name = "special_judge"  <{if $jdg eq "" }>checked<{/if}> /> No 
    <input type = "radio" value = "c" name = "special_judge" <{if $jdg eq "c" }>checked<{/if}> /> C 
    <input type = "radio" value = "cpp" name = "special_judge" <{if $jdg eq "cpp" }>checked<{/if}> /> C++ <br />  
  </td>
</tr>

<tr>
  <th>Judge Program: <{ $jdg_link }></th>
  <td><input type= "file" name="judge_program" size="16" /> <br /> 
  The judge program must take the judge input file of the testdata as arg1, and the output file of users as arg2. <br /> Then it should write the result to stdout, which will then be compared with the output file uploaded. <br /></td>
</tr>

<tr>
  <th>Judge Input: <{ $jdg_input }></th>
  <td><input type= "file" name="jin" size="16" /> <br/></td>
</tr>

<tr><td colspan=2>&nbsp;</td></tr>

  <{section name=id loop=$rs }>
    <tr>
	<td colspan=2>
    <h4><{ $rs[id].title }></h4>
    <{ $rs[id].content }>
    </td>
	</tr>
  <{/section}>
  <br />

<tr><td>
  <input type = "submit" value = "Save" name = "submit"/>
</td></tr>
</form>
</table>
