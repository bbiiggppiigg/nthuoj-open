<?php
/***************************
 * user_rank.php
 * This shows all users' ranking in the system.
 * No parameter is required.
 * *************************/
$cmp = function ($a, $b) {
    if($a['cnt'] > $b['cnt'])   return false;
    elseif($a['cnt'] < $b['cnt'])   return true;
    return ($a['id'] < $b['id']);
};

    session_start();
    include_once("lib/base.php");
    include_once("lib/contest_lib.php");
    include_once("lib/database_tools.php");
    include_once("lib/handler.php");

    $tpl = new Handler("User Ranklist", "user_rank.tpl");
    $con = get_database_object();

    $query = "SELECT id FROM users
              where id Not in (select id from judge) ORDER BY id ASC ";
    $result = mysql_query($query) or die(mysql_error());
  
    $user_status = array();
    while( $row = mysql_fetch_array($result, MYSQL_ASSOC) ) {
        $uid = $row['id'];
		//*
		$query = "SELECT count(distinct SM.pid)
              FROM submission_result_detail as SM, submissions as S 
              WHERE S.uid ='$uid' AND S.sid = SM.sid
                AND (SM.verdict='Accepted')  
              ";
        $rs = mysql_query($query) or die(mysql_error());
        $r1 = mysql_fetch_array($rs);
        $row['cnt'] = $r1[0];
        array_push($user_status, $row);
    }

    usort($user_status, $cmp);
    $num = count($user_status);
	
	for($noneZero = $num-1;$noneZero>0 &&$user_status[$noneZero]['cnt']==0;--$noneZero);
	++$noneZero;
	$bound = array(9,$noneZero*0.05,$noneZero*0.15,$noneZero*0.4,$noneZero,$num);

	$user_status[0]['rank'] = 1;
	$user_status[0]['pic'] = 0;
    for($i=1,$j=0;$i<$num;++$i){
		if($user_status[$i]['cnt']==$user_status[$i-1]['cnt']){
			$user_status[$i]['rank'] = $user_status[$i-1]['rank'];
		}else {
			$user_status[$i]['rank'] = $i+1;
		}
		while($user_status[$i]['rank'] > $bound[$j]){
			++$j;
		}
		$user_status[$i]['pic'] = $j;
	}

    mysql_close($con);
    $tpl->assign("rs", $user_status);
    $tpl->display("base.html");
?> 
