#!/bin/bash

# change directory to where genConfig.sh exists 
cd $(dirname $0)
root=`pwd`
cp $root/original.config /etc/nthuoj/nthuoj.config

read -p "please enter your dispatcher IP: " ip
while [ "$ip" == "" ]
do
	read -p "please enter your dispatcher IP again: " ip
done

sed -i "s/S_IP/$ip/g" /etc/nthuoj/nthuoj.config

new_replace_str=${root//\//\\/}
sed -i "s/ROOT/$new_replace_str/g" /etc/nthuoj/nthuoj.config
