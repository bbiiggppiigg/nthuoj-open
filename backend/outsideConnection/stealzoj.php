#!/usr/bin/php -q
<?php
	require_once ('./simple_html_dom.php');
	require_once ('/var/www/lib/database_tools.php');
	if ($argc < 2) {
		echo 'Parameters Error\n';
		exit;
	}
	$local_sid = $argv[1];          // local submit id
    $con = get_database_object();
    $query = "UPDATE submissions SET status = 'ZOJ Judging' WHERE sid = $local_sid";
    mysql_query($query) or die('Query failed.' . mysql_error());
	$message = 'Judge Error';
    $fp = fopen('./last_zoj', 'r') or die ('Error opening file!');
    if (!($zoj_sid = fscanf($fp, "%d"))) $zoj_sid = 0;
    $zoj_sid = $zoj_sid[0];
    fclose($fp);
	$cputime = 'null';
    $memusage = 'null';
	$count = 20;
	while ($count > 0) {
		--$count;
		sleep(5);
		$html = file_get_html('http://acm.zju.edu.cn/onlinejudge/showRuns.do?contestId=1&search=false&firstId=-1&lastId=-1&handle=24300');
		$ret = $html->find('table.list tr', 1);
        $sid = trim($ret->children(0)->innertext);
        if ($sid <= $zoj_sid) continue;
        $message = trim($ret->children(2)->children(0)->innertext);
        //        if ($message == 'Compiling' || $message == 'Waiting' || $message == 'Running &amp; Judging') continue;
        if ($message == 'Running') continue;
        $memusage = $ret->children(6)->innertext;
        $cputime = $ret->children(5)->innertext;
		//if ($sid <= $zoj_sid) continue;
        $fp = fopen('./last_zoj', 'w') or die ('Error writing file!');
        fprintf($fp, "%d\n", $sid);
        fclose($fp);
        break;
	}
    if($memusage == '') $memusage = 'null';
    if($cputime == '') $cputime = 'null';
    if ($cputime != 'null') $cputime = intval($cputime)/1000.0;
    $query = "UPDATE submissions SET status = '$message', cpu = $cputime, memory = $memusage WHERE sid = $local_sid";
    mysql_query($query) or die('Query failed.' . mysql_error());
    mysql_close($con);
?>
