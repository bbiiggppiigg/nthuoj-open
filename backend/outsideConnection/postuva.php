#!/usr/bin/php -q
<?php
include_once('./simple_html_dom.php');
include_once("/var/www/lib/database_tools.php");

if ($argc != 5) {
    echo 'Parameters Error\n';
    exit;
}
$file = $argv[1];               // source code
$pid = $argv[2];                // pid of local Judge
$language = $argv[3];           // program language
$sid = $argv[4];
$query = "select realid from nthuoj.mapping where pid = '$pid' limit 1";

$con = get_database_object();
$result = mysql_query($query) or die('Query failed.' . mysql_error());
$arr = mysql_fetch_array($result);
$problem = $arr['realid'];
function getUrl($url, $method='', $vars='') {
    $ch = curl_init();
    if ($method == 'post') {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($vars, '', '&'));
    }
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.4 (KHTML, like Gecko) Chrome/6.0.477.0 Safari/534.4");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'uva_cookies');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'uva_cookies');
    $buffer = curl_exec($ch);
    curl_close($ch);
    $tmp = fopen("outcome.txt", "w");
    fprintf($tmp, "%s\n", $buffer);
    fclose($tmp);
    return $buffer;
}

$loginUrl = 'http://uva.onlinejudge.org/index.php';
$html = str_get_html(getUrl($loginUrl));
$loginCount = 10;
//$form = $html->find('form#mod_loginform');
$form = $html->find('form', 0);
while ($form->id == "mod_loginform" && $loginCount--) {
//while (sizeof($form) && $loginCount--) {
    $input = $form->find('input');
    $loginFields = array();
    $loginFields['username'] = '24300';
    $loginFields['passwd'] = '22825NJ';
    $loginFields['option'] = 'com_comprofiler';
    $loginFields['task'] = 'login';
    for ($i = 2; $i <= 11; ++$i)
        $loginFields[$input[$i]->name] = $input[$i]->value;
    $html = str_get_html(getUrl($loginUrl, 'post', $loginFields));
    $form = $html->find('form', 0);//form#mod_loginform');
}
if ($form->id == "mod_loginform") exit;
$code = file_get_contents($file);
$submitUrl = 'http://uva.onlinejudge.org/index.php';
$submitFields = array(
    'option' => 'com_onlinejudge',
    'Itemid' => '25',
    'page' => 'save_submission',
    'problemid' => '',
    'category' => '',
    'localid' => $problem,
    'language' => $language,
    'code' => $code,
    'codeupl' => ''
);

$submit_count = 5;

do {
  echo $submit_count;
  $response = getUrl($submitUrl, 'post', $submitFields);
  $query = "mosmsg=Submission+received+with+ID+";
  $pos1 = strpos($response, $query) + strlen($query);
  $pos2 = strpos($response, '"', $pos1);
  $sub_id = (int)(substr($response, $pos1, $pos2-$pos1));

  if ($sub_id > 0)
    break;

  --$submit_count;

} while ($submit_count > 0);

$sql = "INSERT INTO uva_sid (ssid, uva_sid)
        VALUES ($sid, $sub_id)
        ON DUPLICATE KEY UPDATE uva_sid = $sub_id";
mysql_query($sql) or die(mysql_error());
if($sub_id > 0)
    $query = "UPDATE submissions SET status = 'UVa Judging' WHERE sid = $sid";
else
    $query = "UPDATE submissions SET status = 'Judge Error' WHERE sid = $sid";
mysql_query($query) or die(mysql_error());
sleep(3);

mysql_close($con);
?>
