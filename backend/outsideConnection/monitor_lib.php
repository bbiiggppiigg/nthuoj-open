<?php
    include_once("./simple_html_dom.php");
    include_once("/var/www/lib/base.php");
    include_once("/var/www/lib/database_tools.php");

    $con = get_database_object();
    $index_url = "https://icpcarchive.ecs.baylor.edu/index.php";
    $mypid = getmypid();

    function print_err_msg($err_msg = "")
    {
        fprintf(STDERR, "%s: %s\n", date("Y-m-d H:i:s"), $err_msg);
    }

    function parse_status($status)
    {
        $important_status_list = array(
            "judge error" => "Judge Error",
            "in queue" => "In Queue",
            "sending to judge" => "Sending to Judge",
            "icpc judging" => "ICPC Judging",
            "accepted" => "Accepted",
            "presentation error" => "Presentation Error",
            "wrong answer" => "Wrong Answer",
            "runtime error" => "Runtime Error",
            "time limit exceeded" => "Time Limit Exceeded",
            "memory limit exceeded" => "Memory Limit Exceeded",
            "output limit exceeded" => "Output Limit Exceeded",
            "restricted function" => "Restricted Function",
            "compile error" => "Compile Error",
            "compilation error" => "Compile Error",
            "submission error" => "Submission Error"
        );

        foreach ($important_status_list as $key => $val)
            if (stripos($status, $key) !== false)
                return $val;
        return false;
    }

    function get_url($url, $method = "", $vars = "")
    {
        global $mypid;

        $ch = curl_init();

        if ($method == "post") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($vars, "", "&"));
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT,
                    "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.4 "
                    . "(KHTML, like Gecko) Chrome/6.0.477.0 Safari/534.4");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_COOKIEJAR, "icpc_cookies.$mypid");
        curl_setopt($ch, CURLOPT_COOKIEFILE, "icpc_cookies.$mypid");

        // blindly accept certificate of live archive
        // modified 2013/8/21 10:47
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $buffer = false;
        $buffer = curl_exec($ch);

        if (curl_errno($ch)) {
            print_err_msg("Error " . curl_errno($ch) . ": " . curl_error($ch));
            $buffer = false;
        }

        curl_close($ch);
        return $buffer;
    }

    function check_login_icpc($html)
    {
        return $html !== false && strpos($html, "Hi, 24300") !== false;
    }

    function fetch_icpc_sid($html)
    {
        if ($html === false)
            return false;

        $rgx = "/Submission\+received\+with\+ID\+(\d+)/";
        if (preg_match($rgx, $html, $res) == 0)
            return false;
        return $res[1];
    }

    function login_icpc()
    {
        global $index_url;

        $html = get_url($index_url);
        if (check_login_icpc($html))
            return true;

        $login_fields = array(
            "option" => "com_comprofiler",
            "task" => "login",
            "username" => "24300",
            "passwd" => "22825NJ"
        );

        $html = str_get_html($html);
        foreach ($html->find("input[type=hidden]") as $val)
            if ($val->name != "option")
                $login_fields[$val->name] = $val->value;
        $login_fields["Submit"] = "Login";
        $html->clear();

        $html = get_url($index_url, "post", $login_fields);
        return check_login_icpc($html);
    }

    function post_icpc($icpc_pid, $lang, $code)
    {
        global $index_url;

        if ($lang == "cpp")
            $lang = 3;
        else
            $lang = 1;

        $submit_fields = array(
            "option" => "com_onlinejudge",
            "Itemid" => 25,
            "page" => "save_submission",
            "localid" => $icpc_pid,
            "language" => $lang,
            "code" => $code
        );

        $html = get_url($index_url, "post", $submit_fields);
        return fetch_icpc_sid($html);
    }

    function crawl_icpc()
    {
        global $index_url;

        $crawl_url = $index_url . "?option=com_onlinejudge&Itemid=9&limit=50&limitstart=0";
        $html = get_url($crawl_url);

        if ($html === false)
            return false;

        $html = str_get_html($html);
        $entries = $html->find("tr[class=sectiontableentry1],
                                tr[class=sectiontableentry2]");
        $ret = array();
        foreach($entries as $row) {
            $tmp["icpc_sid"]    = $row->find("td", 0)->plaintext;
            $tmp["status"]      = $row->find("td", 3)->plaintext;
            $tmp["cpu"]         = $row->find("td", 5)->plaintext;
            array_push($ret, $tmp);
        }
        $html->clear();
        return $ret;
    }

    function fetch_unjudged()
    {
        global $con;

        $query = "SELECT sid, realid, source
                  FROM submissions
                  INNER JOIN mapping ON submissions.pid = mapping.pid
                  INNER JOIN problems ON submissions.pid = problems.pid
                  WHERE (status = 'Judging by other Judge' OR status = 'Sending to Judge')
                    AND tid = 1
                  ORDER BY sid ASC LIMIT 1";
        ($result = mysql_query($query, $con)) or die("Query failed in fetching.\n");
        if (!$result || mysql_num_rows($result) == 0)
            $ret = null;
        else
            $ret = mysql_fetch_array($result, MYSQL_ASSOC);
        mysql_free_result($result);
        return $ret;
    }

    function add_mapping($sid, $icpc_sid)
    {
        global $con;

        $query = "INSERT INTO icpc_sid
                  VALUES ($sid, $icpc_sid)
                  ON DUPLICATE KEY UPDATE icpc_sid = $icpc_sid";
        mysql_query($query, $con) or die("Query failed in inserting\n");
    }

	function get_pid($sid)
	{
        global $con;

        $query = "SELECT pid
                  FROM submissions
                  WHERE sid = $sid";
        ($result = mysql_query($query, $con)) or die("Query failed getting sid.\n");
        if (!$result || mysql_num_rows($result) == 0)
            $ret = null;
        else {
            $row = mysql_fetch_array($result, MYSQL_ASSOC);
            $ret = $row["pid"];
        }
        mysql_free_result($result);
        return $ret;
		
	}
    function get_local_sid($icpc_sid)
    {
        global $con;

        $query = "SELECT ssid
                  FROM icpc_sid
                  WHERE icpc_sid = $icpc_sid";
        ($result = mysql_query($query, $con)) or die("Query failed getting sid.\n");
        if (!$result || mysql_num_rows($result) == 0)
            $ret = null;
        else {
            $row = mysql_fetch_array($result, MYSQL_ASSOC);
            $ret = $row["ssid"];
        }
        mysql_free_result($result);
        return $ret;
    }
	function has_judged($sid)
	{
        global $con;

        $query = "SELECT pid
                  FROM submission_result_detail
                  WHERE sid = $sid";
        ($result = mysql_query($query, $con)) or die("Query failed getting sid.\n");
        if (!$result || mysql_num_rows($result) == 0)
            return 0;
      	else
			return 1;
		
	}

    function update_status($sid, $status, $sid_type, $cpu = null)
    {
        global $con;

        if (($status = parse_status($status)) === false)
            return;

        if ($sid_type == "icpc_sid")
            $sid = get_local_sid($sid);
        else if ($sid_type != "sid")
            return;

        if (!is_null($sid)) {
			$pid = get_pid($sid);
			if(has_judged($sid))
				return;
			
            if (is_null($cpu))
                $cpu_str = "cpu = null";
            else
                $cpu_str = "cpu = $cpu";
			if($status!="Sending to Judge"&&$status!="ICPC Judging"){
				$query = "INSERT INTO submission_result_detail (sid, pid, tid,
					verdict, runTime, memoryAmt, errMsg)
        			VALUES('".$sid."','".$pid."','-1', '".$status."', '".$cpu."', '-1', 'null')";	
            	mysql_query($query, $con) or die("Query failed updating.\n");
			
				if ($status == "Accepted")
					$status = "1/1";
				else
					$status = "0/1";
			}
			$query = "UPDATE submissions
                      SET status = '" . $status . "', $cpu_str
                      WHERE sid = $sid
                        AND ( status = 'Sending to Judge'
                              OR status = 'ICPC Judging'
							  OR status = 'Judging by other Judge'
                            )";
			
            mysql_query($query, $con) or die("Query failed updating.\n");
        }
    }

?>

