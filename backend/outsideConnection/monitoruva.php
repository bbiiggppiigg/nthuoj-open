#!/usr/bin/php -q
<?php

function translation_status($vid)
{
    if($vid == 90) return "1/1";
    return "0/1";
}
function translation_submission_detail($vid)
{
    if($vid == 10)  return "Submission error";
    else if($vid == 15) return "Can't be judged";
    else if($vid == 20) return "In Queue";
    else if($vid == 30) return "Compile Error";
    else if($vid == 35) return "Restrict Function";
    else if($vid == 40) return "Runtime Error";
    else if($vid == 45) return "Output Limit Exceeded";
    else if($vid == 50) return "Time Limit Exceeded";
    else if($vid == 60) return "Memory Limit Exceeded";
    else if($vid == 70) return "Wrong Answer";
    else if($vid == 80) return "Presentation Error";
    else if($vid == 90) return "Accepted";
    return "UVa Judging";
}

    include_once("/var/www/lib/database_tools.php");     

    //ini_set('memory_limit', '128M');
    $con = get_database_object();

    while(1) {
        sleep(10);
        $query = "SELECT submissions.sid, submissions.pid, uva_sid.uva_sid, mapping.realid
                  FROM submissions
                    INNER JOIN uva_sid ON uva_sid.ssid=submissions.sid
                    INNER JOIN mapping ON mapping.pid=submissions.pid
                 WHERE (submissions.status='Being Judged' OR submissions.status='UVa Judging')
                    AND uva_sid.uva_sid != 0
                  ORDER BY submissions.sid ASC
                 ";
        $rs = mysql_query($query) or die(mysql_error());
        while( $row = mysql_fetch_array($rs) ) {
            $url = "http://uhunt.felix-halim.net/api/subs-nums/70026/".$row['realid']."/".($row['uva_sid']-1);
            $content = file_get_contents($url);
            $result = json_decode($content, true);
            $subs = $result[70026]['subs'];
            foreach ($subs as $value) {
                if($value[0] == $row['uva_sid']) {
                    $cputime = $value[3];
                    $message = translation_submission_detail($value[2]);
		    		if ($message != "UVa Judging"){
						$query = "INSERT INTO 
							submission_result_detail (sid, pid, tid, verdict, runTime, memoryAmt, errMsg)
							VALUES('".$row['sid']."', '".$row['pid']."', '-3', '".$message."', '".$cputime."', '-1', 'null')";
						mysql_query($query) or die(mysql_error());
						$message = translation_status($value[2]);
						$query = "UPDATE submissions
                				SET status='$message'
                        		WHERE sid=".$row['sid'];
		    		}
					else {
						$query = "UPDATE submissions
                        		SET status='$message'
                        		WHERE sid=".$row['sid'];
					}

					mysql_query($query) or die(mysql_error());

                    break;
                }
            }
        }
    }
    mysql_close($con);
?>
