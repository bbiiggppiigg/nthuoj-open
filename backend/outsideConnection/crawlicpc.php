#!/usr/bin/php -q
<?php
    require_once("monitor_lib.php");

    $slp_ln = 5;
    while (1) {
        sleep($slp_ln);
        if (login_icpc() === false) {
            $slp_ln = 10;
            continue;
        }
        else
            $slp_ln = 5;

        if (($res = crawl_icpc()) !== false) {
            foreach ($res as $row) {
                $icpc_sid   = $row["icpc_sid"];
                $status     = $row["status"]; 
                $cpu        = $row["cpu"];
                update_status($icpc_sid, $status, "icpc_sid", $cpu);
            }
        }
    }

    exit(0);
?>

