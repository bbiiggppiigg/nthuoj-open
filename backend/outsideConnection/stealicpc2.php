#!/usr/bin/php -q
<?php
require_once('./simple_html_dom.php');
require_once('/var/www/lib/database_tools.php');	
if ($argc < 3) {
	echo 'Parameters Error\n';
	exit;
}
function getUrl($url, $method='', $vars='') {
    $ch = curl_init();
    if ($method == 'post') {
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($vars, '', '&'));
    }   
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.4 (KHTML, like Gecko) Chrome/6.0.477.0 Safari/534.4");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'icpc_cookies');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'icpc_cookies');
    $buffer = curl_exec($ch);
    curl_close($ch);
    return $buffer;
}


	$count = $argv[1];              // try times
	$local_sid = $argv[2];          // local submit id
	$con = get_database_object();
	$query = "UPDATE submissions SET status = 'ICPC judging' WHERE sid = $local_sid";
	mysql_query($query) or die('Query failed.' . mysql_error());

    /********************/

    exit(0);

    /********************/

	$message = 'Judge Error';
    $fp = fopen('./last_icpc', 'r') or die ('Error opening file!');
    if (!($uva_sid = fscanf($fp, "%d"))) $uva_sid = 0;
    $uva_sid = $uva_sid[0];
    fclose($fp);
	$cputime = 'null';
    $memusage = 'null';
	while ($count > 0) {
		--$count;
		sleep(5);
		$html = str_get_html(getUrl('http://livearchive.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=9'));
		if (!$html) break;
		$data = $html->find('td.maincontent', 0)->find('table', 0)->find('tr', 1);
        if (sizeof($data->children) < 2) continue;
        $sid = $data->children(0)->innertext;
        if ($sid <= $uva_sid) continue;
        $msg = $data->children(3)->innertext;
        if ($msg == '' || $msg == 'Sent to judge' || $msg == 'Running' || $msg == 'Compiling' || $msg == 'In judge queue' || $msg == 'Linking' || $msg == 'Received') continue;
        $message = $msg;
        $cputime = $data->children(5)->innertext;
        $fp = fopen('./last_uva', 'w') or die ('Error writing file!');
        fprintf($fp, "%d\n", $sid);
        fclose($fp);
        break;
	}
    if($cputime == '') $cputime = 'null';
    if(strpos($message, 'Compilation') !== false) $message = 'Compile Error';
    else if ($message == 'Wrong answer') $message = 'Wrong Answer';
    $query = "UPDATE submissions SET status = '$message', cpu = $cputime, memory = $memusage WHERE sid = $local_sid";
    mysql_query($query) or die('Query failed.' . mysql_error());
    mysql_close($con);
?>
