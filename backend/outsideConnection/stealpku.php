#!/usr/bin/php -q
<?php
	require_once ('./simple_html_dom.php');
	require_once ('/var/www/lib/database_tools.php');
    
    ini_set('memory_limit', '128M');

	if ($argc < 2) {
		echo 'Parameters Error\n';
		exit;
	}

	$local_sid = $argv[1];          // local submit id
    $con = get_database_object();
    $query = "UPDATE submissions SET status = 'POJ Judging' WHERE sid = $local_sid";
    mysql_query($query) or die('Query failed.' . mysql_error());
	$message = 'Judge Error';
    $fp = fopen('./last_pku', 'r') or die ('Error opening file!');
    if (!($pku_sid = fscanf($fp, "%d"))) $pku_sid = 0;
    $pku_sid = $pku_sid[0];
    fclose($fp);
	$cputime = 'null';
    $memusage = 'null';
	$count = 20;
	while ($count > 0) {
		--$count;
		sleep(5);
		$html = file_get_html('http://poj.org/status?problem_id=&user_id=24300GO&result=&language=');
		$ret = $html->find('table', 4);
        if (sizeof($ret->children) < 2) continue;
        $ret = $ret->children(1);
        $sid = $ret->children(0)->innertext;
        if ($sid <= $pku_sid) continue;
        $msg = $ret->children(3)->children(0)->innertext;
        if (strpos($msg, 'Running') !== false) $msg = 'Running';
        if ($msg == 'Compiling' || $msg == 'Waiting' || $msg == 'Running') continue;
        $message = $msg;
        $memusage = $ret->children(4)->innertext;
        $cputime = $ret->children(5)->innertext;
		if ($sid <= $pku_sid) continue;
        $fp = fopen('./last_pku', 'w') or die ('Error writing file!');
        fprintf($fp, "%d\n", $sid);
        fclose($fp);
        break;
	}
    $memusage = str_replace('K', '', $memusage);
    $cputime = str_replace('MS', '', $cputime);
    if($memusage == '') $memusage = 'null';
    if($cputime == '') $cputime = 'null';
    if ($cputime != 'null') $cputime = intval($cputime)/1000.0;
    if (strpos($message, 'Compile') !== false) $message = 'Compile Error';
	$query = "INSERT INTO submission_result_detail (sid, tid, verdict, runTime, memoryAmt, errMsg)
		VALUES('".$local_sid."','-2', '".$message."', '".$cputime."', '".$memusage."', 'null')";
	mysql_query($query) or die(mysql_error());
	if($message == 'Accepted') 
    	$query = "UPDATE submissions SET status = '1/1' WHERE sid = $local_sid";
    else
		$query = "UPDATE submissions SET status = '0/1' WHERE sid = $local_sid";

	mysql_query($query) or die(mysql_error());
    mysql_close($con);
?>
