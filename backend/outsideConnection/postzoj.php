#!/usr/bin/php -q
<?php
require_once ('./simple_html_dom.php');
require_once ('/var/www/lib/database_tools.php');
if ($argc < 4) {
    echo 'Parameters Error\n';
    exit;
}
$file = $argv[1];                   // source code
$pid = $argv[2];                // pid of local Judge
$language = $argv[3];               // program language
$query = "select realid from nthuoj.mapping where pid = '$pid' limit 1";
$con = get_database_object();
$result = mysql_query($query) or die('Query failed.' . mysql_error());
$arr = mysql_fetch_array($result);
$problem = $arr['realid'];
mysql_close($con);
function getUrl($url, $method='', $vars='') {
    $ch = curl_init();
    if ($method == 'post') {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($vars, '', '&'));
    }
    curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.4 (KHTML, like Gecko) Chrome/6.0.477.0 Safari/534.4");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'zoj_cookies');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'zoj_cookies');
    $buffer = curl_exec($ch);
    curl_close($ch);
    return $buffer;
}    
$code = file_get_contents($file);
$loginUrl = 'http://acm.zju.edu.cn/onlinejudge/login.do'; //action from the login form
$loginFields = array('handle'=>'24300', 'password'=>'22825NJ'); //login form field names and values
$problemUrl = 'http://acm.zju.edu.cn/onlinejudge/showProblem.do?problemCode='.$problem;
$remotePageUrl = 'http://acm.zju.edu.cn/onlinejudge/submit.do'; //url of the page you want to save  
$test = str_get_html(getUrl($remotePageUrl));
if ($test->find("div[id=content_title]", 0)->innertext == "Login")
	getUrl($loginUrl, 'post', $loginFields); //login to the site
$prob = str_get_html(getUrl($problemUrl));
$problemid =  substr($prob->find('center a', 0)->href, 33);
$submitFields = array('problemId'=>$problemid, 'languageId'=>$language, 'source'=>$code);
getUrl($remotePageUrl, 'post', $submitFields);
?>
