<?php
/****************************
resultUpdater.php
This get POST parameters about a judge result of a submission and update the result to database. 
Input POST parameters: 'sid' (submission ID), 'pid' (problem ID), 'verdict' (result Point: a/b), 'runTime' , 'memoryAmt' (memory amount), 'errMsg', 'machineName'
*****************************/
	session_start();
	require_once("lib/database_tools.php");
	require_once("dispatcherFunction.php");
	$con = get_database_object();
	
	writeLog("updater start");
	$sid = $_POST['sid'];
	$pid = $_POST['pid'];
	$tid = $_POST['tid'];
	$verdict = $_POST['verdict'];
	$runTime = $_POST['runTime'];
	$memoryAmt = $_POST['memoryAmt'];
	$errMsg = $_POST['errMsg'];
	$machineName = $_POST['machineName'];
	$tidNum = count($tid);
	$verdictNum = count($verdict);
	$i=0;
	$ac=0;
	foreach ($verdict as $value)
	{
		echo "verdict ".$i.":".$value."<br>";
		if(!strcmp($value,"Accepted"))
			$ac++;
        $i++;
	}
	$i=0;
	foreach ($runTime as $value)
	{
		echo "runtime ".$i.":".$value."<br>";
	    $i++;
	}
	$i=0;
	foreach ($memoryAmt as $value)
	{
		echo "memoryAmt ".$i.":".$value."<br>";
	    $i++;
	}
	/*$i=0;
	foreach ($errMsg as $value)
	{
		echo "errMsg ".$i.":".$value."<br>";
	    $i++;
	}*/
	echo $machineName."<br>";
	setMachineStatus($machineName, 0);
	writeLog("sid = ".$sid."insert submission detail table tidNum = ".$tidNum);
	$i=0;
	$err = str_replace('\\', '\\\\', $errMsg[0]);
	$err = str_replace('\'', '\\\'', $err);
	while($i < $verdictNum)
	{
		$sql = "INSERT INTO  submission_result_detail (sid, pid, tid, verdict, runTime, memoryAmt)VALUES('".$sid."', '".$pid."', '".$tid[$i]."', '".$verdict[$i]."', '".$runTime[$i]."', '".$memoryAmt[$i]."')";
		//$sql = "INSERT INTO submission_result_detail (sid, pid, tid, verdict, runTime, memoryAmt, errMsg) VALUES(10000, 1000, 1000, 'Accept', 10.0, 10, 'ERR')";
		writeLog($sql);
		echo $sql;
		mysql_query($sql) or die(mysql_error());
		$i++;
	}
	writeLog("update submission table");
	$sql = "UPDATE submissions SET status = '".$ac."/".$tidNum."', err_msg = '".$err."'  WHERE sid = ".$sid;
	mysql_query($sql) or die(mysql_error());
?>
